BEGIN;

DROP TABLE IF EXISTS "department";

CREATE TABLE IF NOT EXISTS "department"(
  "code" VARCHAR(3) PRIMARY KEY NOT NULL DEFAULT '',
  "denomination" VARCHAR(255) DEFAULT NULL,
  "slug" VARCHAR(255) DEFAULT NULL
);
CREATE INDEX "department.index_denomination" ON "department"("denomination");

DROP TABLE IF EXISTS "city";

CREATE TABLE IF NOT EXISTS "city"(
  "uid" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL CHECK("uid">=0),
  "department_uid" VARCHAR(3) NOT NULL,
  "slug" VARCHAR(255) DEFAULT NULL,
  "uppercase" VARCHAR(45) DEFAULT NULL,
  "denomination" VARCHAR(45) DEFAULT NULL,
  "zipcode" VARCHAR(255) DEFAULT NULL,
  "municipality" VARCHAR(3) DEFAULT NULL,
  "code" VARCHAR(5) NOT NULL,
  "district" INTEGER CHECK("district">=0) DEFAULT NULL,
  "canton" VARCHAR(4) DEFAULT NULL,
  "longitude" FLOAT DEFAULT NULL,
  "latitude" FLOAT DEFAULT NULL,
  CONSTRAINT "fk_city_department"
    FOREIGN KEY("department_uid")
    REFERENCES "department"("code")
    ON DELETE RESTRICT
    ON UPDATE RESTRICT
);
CREATE INDEX "city.index_uppercase_denomination" ON "city"("uppercase");
CREATE INDEX "city.index_denomination" ON "city"("denomination");
CREATE INDEX "city.index_zipcode" ON "city"("zipcode");
CREATE INDEX "city.index_longitude" ON "city"("longitude");
CREATE INDEX "city.index_latitude" ON "city"("latitude");
CREATE INDEX "city.fk_city_department_idx" ON "city"("department_uid");

COMMIT;