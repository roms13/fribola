/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 03/12/2015
 * Time: 23:01
 */
package com.kroms.fribola.database
{
    public class Column
    {
        public var allowNull            : Boolean;
        public var autoIncrement        : Boolean;
        public var defaultCollationType : String;

        public var dataType             : String;
        public var name                 : String;
        public var primaryKey           : Boolean;

        public var max                  : String;
        public var unique               : Boolean;
    }
}
