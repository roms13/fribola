/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 02/03/2015
 * Time: 16:29
 */
package com.kroms.fribola.database
{

    /**
     * interface for storage column
     * e.g : a database table column
     */
    public interface IColumn
    {
        function get value () : String;
        function set value ( value : String ) : void;
        function get name () : String;
        function set name ( value : String ) : void;
    }
}
