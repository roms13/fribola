/**
 * Created by Romain KELIFA on 03/01/2015.
 */
package com.kroms.fribola.database
{
    import avmplus.getQualifiedClassName;

    import com.kroms.fribola.data.core.City;
    import com.kroms.fribola.error.SingletonError;

    /**
     * table 'city' from database 'library'
     * contains all data from french cities
     */
    public class CityTable extends Table
    {
        static private var _instance : CityTable;

        static private const ENFORCER : Object = {};

        static public const NAME : String = "city";

        override public function get name () : String
        {
            return NAME;
        }

        override public function get itemClass () : Class
        {
            return City;
        }

        public function CityTable( enforcer : Object )
        {
            super();

            if ( !enforcer || enforcer != ENFORCER )
                throw new SingletonError(getQualifiedClassName(this));
        }

        static public function getInstance () : CityTable
        {
            if ( !_instance )
                _instance = new CityTable( ENFORCER );

            return _instance;
        }

        override public function get columns () : Vector.<String>
        {
            return new <String>[
                "uid",
                "denomination",
                "zipcode",
                "department_uid"
            ];
        }
    }
}
