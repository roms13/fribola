/**
 * Created by Romain KELIFA on 03/01/2015.
 */
package com.kroms.fribola.database
{
    import avmplus.getQualifiedClassName;

    import com.kroms.fribola.data.core.Department;
    import com.kroms.fribola.error.SingletonError;

    /**
     * table 'department' from database 'library'
     * contains all data from french departments
     */
    public class DepartmentTable extends Table
    {
        static private var _instance : DepartmentTable;

        static private const ENFORCER : Object = {};

        static public const NAME : String = "department";

        override public function get name () : String
        {
            return NAME;
        }

        override public function get itemClass () : Class
        {
            return Department;
        }

        public function DepartmentTable( enforcer : Object )
        {
            super();

            if ( !enforcer || enforcer != ENFORCER )
                throw new SingletonError(getQualifiedClassName(this));
        }

        static public function getInstance () : DepartmentTable
        {
            if ( !_instance )
                _instance = new DepartmentTable( ENFORCER );

            return _instance;
        }
        
        override public function get columns () : Vector.<String>
        {
            return new <String>[
                "code",
                "denomination",
                "slug"
            ];
        } 
    }
}
