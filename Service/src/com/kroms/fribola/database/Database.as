/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 03/12/2015
 * Time: 21:46
 */
package com.kroms.fribola.database
{
    import flash.data.SQLConnection;
    import flash.data.SQLTableSchema;
    import flash.events.EventDispatcher;
    import flash.filesystem.File;
    import flash.net.Responder;

    public class Database extends EventDispatcher implements IDatabase
    {
        protected var connection : SQLConnection = new SQLConnection();

        public function Database ()
        {
            super();
        }

        public function open () : void
        {
            connection.open( file );
        }

        public function close () : void
        {
            if ( connected )
                connection.close();
        }

        public function loadTableSchema ( name : String, responder : Responder ) : void
        {
            if ( connected )
                connection.loadSchema( SQLTableSchema, name, alias, true, responder );
        }

        public function get connected () : Boolean { return connection.connected; }
        public function get name () : String { return file.name; }

        public function get file () : File { return null; }
        public function get alias () : String { return "main"; }
        public function get tables () : Vector.<ITable> { return null; }
    }
}
