/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 03/12/2015
 * Time: 21:54
 */
package com.kroms.fribola.database
{

    import com.kroms.fribola.term.SQL;
    import com.kroms.fribola.util.SQLUtil;
    import com.kroms.fribola.util.StringUtil;

    import flash.data.SQLStatement;
    import flash.events.EventDispatcher;
    import flash.net.Responder;
    import flash.utils.describeType;

    public class Table extends EventDispatcher implements ITable
    {
        public function Table ()
        {
            super();
        }

        protected function getCreateStatement ( sql : String ) : SQLStatement
        {
            if ( !sql )
            {
                var columns : Vector.<Column> = getSQLColumnsByClass();
                sql = getCreateSqlByColumns( columns );
            }

            return getStatement( sql );
        }

        protected function getInsertStatement ( columns : Vector.<String> = null ) : SQLStatement
        {
            var statement : SQLStatement;

            if ( !columns ) columns = this.columns;

            var c : Vector.<String> = columns ? columns.slice() : null;
            var has_columns : Boolean = c && c.length > 0;

            if ( has_columns )
            {
                statement = new SQLStatement();

                var v : Vector.<String> = c.slice();

                c.forEach( SQLUtil.quoteEach );
                v.forEach( SQLUtil.aliasEach );

                var concatenated_columns : String = c.join(',');
                var concatenated_values : String = v.join(',');

                var request : Vector.<String> = new <String>[];
                request.push( SQL.INSERT + ' ' + SQL.INTO, database.alias + name );
                request.push( SQL.PARENTHESIS_OPEN, concatenated_columns, SQL.PARENTHESIS_CLOSE );
                request.push( SQL.VALUES );
                request.push( SQL.PARENTHESIS_OPEN, concatenated_values, SQL.PARENTHESIS_CLOSE );

                statement.text = request.join(' ');
            }

            return statement;
        }

        protected function getUpdateStatement ( where : Vector.<String>, columns : Vector.<String> = null ) : SQLStatement
        {
            var statement : SQLStatement;

            if ( !columns ) columns = this.columns;

            var c : Vector.<String> = columns ? columns.slice() : null;
            var count : int = c ? c.length : 0;
            var has_columns : Boolean = count > 0;

            if ( has_columns )
            {
                statement = new SQLStatement();

                var v : Vector.<String> = c.slice();

                c.forEach( SQLUtil.quoteEach );
                v.forEach( SQLUtil.aliasEach );

                var e : Array = [];
                var i : int;

                for ( i = 0 ; i < count ; i++ )
                {
                    e.push( c[i] + ' ' + SQL.EQUALS + ' ' + v[i] );
                }

                var concatenated_equalities : String = e.join(',');

                var request : Vector.<String> = new <String>[];
                request.push( SQL.UPDATE, database.alias + name );
                request.push( SQL.SET, concatenated_equalities );
                request.concat( where );

                statement.text = request.join(' ');
            }

            return statement;
        }

        protected function getSQLColumnsByClass( clazz : Class = null ) : Vector.<Column>
        {
            if ( !clazz ) clazz = itemClass;

            if ( !clazz )
                return null;

            var c : Vector.<Column> = new <Column>[];
            var definition : * = describeType ( clazz );
            var properties : XMLList = definition..variable + definition..accessor;
            var property : XML;
            var column : Column;
            var n : String;
            var access : String;
            var as3_type : String;
            var sql_type : String;
            var readonly : Boolean;
            var untyped : Boolean;

            for each ( property in properties )
            {
                n = String ( property.@name );
                access = String ( property.@access );
                readonly = ( access == 'readonly' );
                as3_type = property.@type;
                sql_type = SQLUtil.getSQLDataType ( as3_type );
                untyped = ( sql_type == null );

                if ( readonly || untyped )
                {
                    continue;
                }

                column = new Column ();
                column.allowNull = true;
                column.dataType = sql_type;
                column.allowNull = true;

                c.push ( column );
            }

            return c;
        }

        protected function getStatement ( sql : String ) : SQLStatement
        {
            if ( !sql )
                return null;

            var statement : SQLStatement = new SQLStatement();
            statement.text = sql;

            return statement;
        }

        protected function getCreateSqlByColumns ( columns : Vector.<Column> = null ) : String
        {
            if ( !columns )
                return null;

            var request : Array = [];
            request.push( SQL.CREATE, SQL.TABLE, SQL.IF_NOT_EXISTS, name );
            request.push( SQL.PARENTHESIS_OPEN );

            var count : int = columns ? columns.length : 0;
            var column : Column;
            var i : int = 0;
            var definitions : Array = [];
            var definition : String;
            var part : Array;

            for ( ; i < count ; i++ )
            {
                column = columns[i];
                part = [];
                part.push( column.name, column.dataType);

                if ( StringUtil.isNotEmpty( column.max ) )
                    part.push( SQL.PARENTHESIS_OPEN, column.max, SQL.PARENTHESIS_CLOSE );

                if ( column.primaryKey )
                    part.push( SQL.PRIMARY_KEY );

                if ( column.autoIncrement )
                    part.push( SQL.AUTO_INCREMENT );

                if ( column.unique )
                    part.push( SQL.UNIQUE );

                part.push( column.allowNull );

                if ( StringUtil.isNotEmpty( column.defaultCollationType ) )
                    part.push( column.defaultCollationType );

                definition = part.join(' ');
                definitions.push( definition );
            }

            request.push( definitions.join(',') );
            request.push( SQL.PARENTHESIS_CLOSE );

            var sql : String = request.join(' ');
            return sql;
        }

        public function loadSchema ( responder : Responder ) : void
        {
            database.loadTableSchema( name, responder );
        }

        public function get connected () : Boolean { return database.connected; }
        public function get database () : Database { return null; }
        public function get name () : String { return null; }
        public function get columns () : Vector.<String> { return null; }
        public function get itemClass () : Class { return null; }
    }
}
