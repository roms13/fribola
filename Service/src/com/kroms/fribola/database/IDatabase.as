/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 03/12/2015
 * Time: 21:27
 */
package com.kroms.fribola.database
{

    import flash.net.Responder;

    public interface IDatabase
    {
        function get connected () : Boolean;
        function loadTableSchema ( name : String, responder : Responder ) : void
    }
}
