/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 14:58
 */
package com.kroms.fribola.task
{
    import com.kroms.fribola.data.core.IFile;
    import com.kroms.fribola.data.core.IZip;
    import com.kroms.fribola.task.abstract.Task;

    import flash.utils.ByteArray;

    import org.as3commons.zip.Zip;

    /**
     * create a <code>Zip</code> archive from sources
     * @see org.as3commons.zip.Zip
     */
    public class ZipTask extends Task implements IZip
    {
        public function ZipTask ( source : Vector.<IFile>, required : Boolean = true )
        {
            super ( source, required );
        }

        override protected function process () : void
        {
            super.process ();

            var count : int = iFiles ? iFiles.length : 0;
            var i : int = 0;
            var iFile : IFile;
            var zip : Zip = new Zip();

            var extension   : String;
            var filename    : String;
            var bytes       : ByteArray;
            var compress    : Boolean;

            for ( ; i < count; i++ )
            {
                iFile = iFiles[i];

                extension   = iFile.extension;
                filename    = iFile.filename + '.' + extension;
                bytes       = iFile.bytes;
                compress    = ( extension != 'jpg' );

                zip.addFile( filename, bytes, compress );
            }

            zip.close();
            result = zip;
        }

        private function get iFiles () : Vector.<IFile> { return source as Vector.<IFile>; }
        public function get zip () : Zip { return result as Zip; }
    }
}
