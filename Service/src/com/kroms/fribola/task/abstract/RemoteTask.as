package com.kroms.fribola.task.abstract
{
    import mx.rpc.IResponder;
    import mx.rpc.Responder;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;

    /**
     * abstract task for remote service call implementation
     */
    public class RemoteTask extends AsyncTask
    {
        protected var responder : IResponder;

        public function RemoteTask ( source : Object, required : Boolean = false )
        {
            super ( source, required );
        }

        /**
         * creates a responder for incoming remote service call
         */
        override protected function preProcess () : void
        {
            super.preProcess ();

            responder = new Responder(onResult,onFault);
        }

        /**
         * deletes the formerly used responder
         */
        override protected function postProcess () : void
        {
            super.postProcess ();

            responder = null;
        }

        /**
         * @inheritDoc
         * @see Error
         * @see flash.events.ErrorEvent
         * @see mx.rpc.events.FaultEvent
         */
        override protected function onFault ( e : Object ) : void
        {
            if ( e is FaultEvent )
            {
                var event : FaultEvent = e as FaultEvent;
                fault = event;
                errorMessage = "Erreur du service distant: " + event.type + " > " + event.fault.message + "(" + event.fault.errorID + ")";

                CONFIG::debug
                {
                    trace(errorMessage);
                }
            }

            super.onFault ( e );
        }

        /**
         * handle the result from the remote service call
         * @param e the event dispatched
         */
        protected function onResult ( e : ResultEvent ) : void
        {
            postProcess();
            finish();
        }
    }
}
