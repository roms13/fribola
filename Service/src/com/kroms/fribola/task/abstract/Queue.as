package com.kroms.fribola.task.abstract
{
    import com.kroms.fribola.event.task.TaskEvent;

    /**
     * abstract queue task implementation for any task that needs to process many identical tasks
     * main implementation difference with <code>Sequence</code> is that it's less flexible and not meant to handle each individual task processing
     * here the tasks are only executed one after another
     * @see com.kroms.fribola.task.abstract.Sequence
     * @see com.kroms.fribola.task.abstract.ITask
     */
    public class Queue extends ManyTask implements IQueue
    {
        protected var queue : Vector.<ITask>;
        protected var done : Vector.<ITask>;

        public function Queue ( source : Object, required : Boolean = false )
        {
            super ( source, required );
        }

        /**
         * creates the queue of tasks to be processed and another one to store the tasks once processed
         */
        override protected function preProcess () : void
        {
            super.preProcess ();

            this.queue = new <ITask>[];
            this.done = new <ITask>[];
        }

        /**
         * automatically executes the next task in the queue
         */
        override protected function process () : void
        {
            super.process ();
            next();
        }

        /**
         * @inheritDoc
         */
        override protected function onTask ( e : TaskEvent ) : void
        {
            if ( e.type == TaskEvent.FINISH )
            {
                var task : ITask = e.currentTarget as ITask;
                done.push( task );
            }

            super.onTask(e);
        }

        /**
         * @inheritDoc
         */
        override public function get hasNext () : Boolean
        {
            return ( queue && queue.length > 0 );
        }

        /**
         * @inheritDoc
         */
        override protected function getNext () : ITask
        {
            return queue.pop();
        }

        /**
         * @inheritDoc
         * @return <code>true</code> if every single task is successful, <code>false</code> otherwise
         */
        override public function get successful () : Boolean
        {
            var success : Boolean;
            var count : int = done ? done.length : 0;
            var i : int = 0;
            var task : ITask;

            for ( ; i < count ; i++ )
            {
                task = done[i];
                success = task.successful;
                if ( !success )
                    return false;
            }

            return success;
        }

        /**
         * @inheritDoc
         * @return <code>true</code> if any single task failed, <code>false</code> otherwise
         */
        override public function get failed () : Boolean
        {
            var count : int = done ? done.length : 0;
            var i : int = 0;
            var task : ITask;

            for ( ; i < count ; i++ )
            {
                task = done[i];
                if ( task.failed )
                    return true;
            }

            return false;
        }
    }
}
