package com.kroms.fribola.task.abstract
{
    import avmplus.getQualifiedClassName;

    import com.kroms.fribola.event.task.TaskEvent;

    import flash.events.EventDispatcher;
    import flash.utils.getDefinitionByName;
    import flash.utils.getTimer;

    /**
     * abstract task implementation
     * @see com.kroms.fribola.task.abstract.ITask
     */
    [Event(name="begin",type="com.kroms.fribola.event.task.TaskEvent")]
    [Event(name="finish",type="com.kroms.fribola.event.task.TaskEvent")]
    public class Task extends EventDispatcher implements ITask
    {
        protected var source        : Object;
        protected var result        : Object;
        protected var fault         : Object;
        protected var required      : Boolean;
        protected var errorMessage  : String;

        protected var started_at    : Number;
        protected var ended_at      : Number;

        /**
         * constructor
         * @param source data to be processed
         * @param required (optional) if task is mandatory or not
         */
        public function Task ( source : Object, required : Boolean = true )
        {
            super ();
            this.required = required;
            this.source = source;
        }

        /**
         * @inheritDoc
         */
        public function execute () : void
        {
            begin();

            try
            {
                preProcess();
                process();
                postProcess();
            }
            catch(e:Error)
            {
                onFault(e);
            }

            finish();
        }

        /**
         * begins processing
         */
        protected function begin () : void
        {
            started_at = getTimer();
            dispatchEvent( new TaskEvent ( TaskEvent.BEGIN ) );
        }

        /**
         * if there is any specific pre-process action
         * (as defining variables or adding event listeners for example)
         */
        protected function preProcess () : void
        {}

        /**
         * actual process
         */
        protected function process () : void
        {}

        /**
         * if there is any specific post-process action
         * (as deleting variables or removing event listeners for example)
         */
        protected function postProcess () : void
        {}

        /**
         * finishes process
         */
        protected function finish () : void
        {
            ended_at = getTimer();

            trace( getDefinitionByName( getQualifiedClassName( this ) ) + " processed in " + ( duration / 1000 ).toString() + " second(s)" );

            dispatchEvent( new TaskEvent ( TaskEvent.FINISH ) );
        }

        /**
         * if any error occurs during process, stores the error and finishes the process
         *
         * @param e the error
         * @see Error
         */
        protected function onFault ( e : Object ) : void
        {
            if ( e is Error )
            {
                var error : Error = e as Error;
                fault = error;
                errorMessage = "Erreur d'exécution: " + error.name + " > " + error.message + "(" + error.errorID + ")";

                CONFIG::debug
                {
                    trace(errorMessage);
                }
            }

            postProcess();
            finish();
        }

        /**
         * @inheritDoc
         */
        public function get successful () : Boolean { return result != null; }

        /**
         * @inheritDoc
         */
        public function get failed () : Boolean { return !successful && fault; }

        /**
         * @inheritDoc
         */
        public function get error () : Object { return fault != null; }

        /**
         * @inheritDoc
         */
        public function get mandatory () : Boolean { return required; }

        /**
         * @inheritDoc
         */
        public function get processing () : ITask { return this; }

        /**
         * @inheritDoc
         */
        public function get processable () : Boolean { return source != null; }

        /**
         * @inheritDoc
         */
        public final function get duration () : Number
        {
            return ( ( isNaN( started_at ) || isNaN( ended_at ) ) ? Number.NaN : ( ended_at - started_at ) );
        }
    }
}
