package com.kroms.fribola.task.abstract
{
    /**
     * abstract sequence task implementation for any task that needs to process inner consecutive tasks
     * main implementation difference with <code>Queue</code> is that it's more flexible and not meant to execute the same tasks over and over
     * here the developer has the responsability to handle each task individually, their result, etc
     * @see com.kroms.fribola.task.abstract.Queue
     * @see com.kroms.fribola.task.abstract.ITask
     */
    public class Sequence extends ManyTask implements ISequence
    {
        public function Sequence ( source : Object, required : Boolean = false )
        {
            super ( source, required );
        }

        /**
         * creates the first task and defines it as currently processing
         */
        override protected function preProcess () : void
        {
            super.preProcess ();

            var first : ITask = getFirst();

            if ( first )
            {
                updateCurrent(first);
            }
        }

        /**
         * as long as there is any current task, executes it automatically, otherwise finishes the process
         */
        override protected function process () : void
        {
            super.process();

            if ( current && current.processable ) current.execute();
            else finish();
        }

        /**
         * indicates which is the first task to process
         * @return the first task
         * @see com.kroms.fribola.task.abstract.ITask
         */
        protected function getFirst () : ITask { return null; }
    }
}
