package com.kroms.fribola.task.abstract
{
    import flash.events.IEventDispatcher;

    /**
     * interface for every task
     */
    public interface ITask extends IEventDispatcher
    {
        /**
         * executes the task
         */
        function execute () : void;

        /**
         * indicates if task is successful
         * (usually the task is considered as successful as long as a result is set)
         *
         * @return <code>true</code> if successful, <code>false</code> if failed
         */
        function get successful () : Boolean;

        /**
         * indicates if an error occured during execution
         * (usually the task is considered as failed as long as a result is missing and some error is set)
         *
         * @return <code>true</code> if failed, <code>false</code> otherwise
         */
        function get failed () : Boolean;

        /**
         * renvoie l'erreur de traitement
         *
         * @return l'erreur ou <code>null</code>
         */
        function get error () : Object;

        /**
         * determines whether the execution is mandatory (for example in case you executes many interdependent tasks)
         *
         * @return <code>true</code> if required, <code>false</code> is facultative
         */
        function get mandatory () : Boolean;

        /**
         * determines whether the task can be processed or not (in case the source is not valid for example)
         *
         * @return <code>true</code> if processable, <code>false</code> otherwise
         */
        function get processable () : Boolean;

        /**
         * retrieve the currently processed task (useful if task executes many inner tasks)
         *
         * @return the task currently processing
         */
        function get processing () : ITask;

        /**
         * indicates the duration of the process
         *
         * @return task's duration in millisecond(s), or <code>Number.NaN</code> if not finished yet
         */
        function get duration () : Number
    }
}
