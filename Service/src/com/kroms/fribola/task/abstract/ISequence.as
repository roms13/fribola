package com.kroms.fribola.task.abstract
{
    /**
     * interface for task processing consecutive inner tasks
     */
    public interface ISequence extends ITask
    {}
}
