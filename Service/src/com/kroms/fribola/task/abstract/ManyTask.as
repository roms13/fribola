package com.kroms.fribola.task.abstract
{
    import com.kroms.fribola.event.task.TaskEvent;

    /**
     * abstract multiple task implementation, which is to say any task that process many inner tasks
     * @see com.kroms.fribola.task.abstract.ITask
     */
    public class ManyTask extends AsyncTask
    {
        protected var current : ITask;

        /**
         * @inheritDoc
         */
        public function ManyTask ( source : Object, required : Boolean = true )
        {
            super ( source, required );
        }

        /**
         * listens to each task dispatched change
         * @param e
         */
        protected function onTask ( e : TaskEvent ) : void
        {
            if ( e.type == TaskEvent.FINISH )
            {
                var task : ITask = e.currentTarget as ITask;
                unwatch(task);

                // always update result, if required, before carrying on the next task
                updateResult(task);

                if ( task.successful || !task.mandatory )
                {
                    next();
                }
                else
                {
                    fault = task.error;
                    finish();
                }
            }
        }

        /**
         * defines the current task to process, automatically adds listener to it
         * @param task the task to be processed
         */
        protected function updateCurrent ( task : ITask ) : void
        {
            if ( task )
            {
                current = task;
                watch(task);
                dispatchEvent( new TaskEvent( TaskEvent.UPDATE ) );
            }
        }

        /**
         * utility method to add listeners to a task
         * @param task the task to listen to
         */
        protected function watch ( task : ITask ) : void
        {
            if ( task )
            {
                task.addEventListener( TaskEvent.BEGIN,  onTask );
                task.addEventListener( TaskEvent.UPDATE, onTask );
                task.addEventListener( TaskEvent.FINISH, onTask );
            }
        }

        /**
         * utility method to remove listeners from a task
         * @param task the task to stop listening to
         */
        protected function unwatch ( task : ITask ) : void
        {
            if ( task )
            {
                task.removeEventListener( TaskEvent.BEGIN,  onTask );
                task.removeEventListener( TaskEvent.UPDATE, onTask );
                task.removeEventListener( TaskEvent.FINISH, onTask );
            }
        }

        /**
         * @inheritDoc
         */
        override public function get processing () : ITask
        {
            return current;
        }

        /**
         * automatically executes the next task, if it exists and is processable
         */
        protected function next () : void
        {
            if ( hasNext )
            {
                var next : ITask = getNext();

                // double check because hasNext() and getNext() can be overriden
                if ( next )
                {
                    updateCurrent(next);
                    next.execute();
                }
                else finish();
            }
            else finish();
        }

        /**
         * determines whether there is any task left to process with
         *
         * @return <code>true</code> if there is any task to be processed, <code>false</code> otherwise
         */
        public function get hasNext () : Boolean { return true; }

        /**
         * returns the next task to process with
         *
         * @return the next task to be processed
         */
        protected function getNext () : ITask { return null; }

        /**
         * automatically update the result from process, if required
         * @param task the processed task to retrieve the result from
         */
        protected function updateResult ( task : ITask ) : void {}
    }
}
