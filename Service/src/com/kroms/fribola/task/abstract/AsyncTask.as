package com.kroms.fribola.task.abstract
{
    import flash.events.ErrorEvent;

    /**
     * abstract asynchronous task implementation, which is to say one which cannot be processed inside one method only
     */
    public class AsyncTask extends Task
    {
        /**
         * @inheritDoc
         */
        public function AsyncTask ( source : Object, required : Boolean = true )
        {
            super ( source, required );
        }

        /**
         * executes the task without finishing it, as it is asynchronous (developer has the responsability to manually notify that process is finished)
         */
        override public function execute () : void
        {
            begin();

            try
            {
                preProcess();
                process();
                // no call to postProcess() method as it's an asynchronous process
            }
            catch(e:Error)
            {
                onFault(e);
            }

            // no call to finish() method as it's an asynchronous process
        }

        /**
         * @inheritDoc
         * @see Error
         * @see flash.events.ErrorEvent
         */
        override protected function onFault ( e : Object ) : void
        {
            if ( e is ErrorEvent )
            {
                var event : ErrorEvent = e as ErrorEvent;
                fault = event;
                errorMessage = "Processing error: " + event.type + " > " + event.text + "(" + event.errorID + ")";

                CONFIG::debug
                {
                    trace(errorMessage);
                }
            }

            super.onFault ( fault );
        }
    }
}
