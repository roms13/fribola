package com.kroms.fribola.task.abstract
{
    /**
     * interface for task processing inner queued tasks
     */
    public interface IQueue extends ITask
    {}
}
