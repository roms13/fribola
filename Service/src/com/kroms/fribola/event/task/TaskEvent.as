package com.kroms.fribola.event.task
{
    import flash.events.Event;

    public class TaskEvent extends Event
    {
        static public const BEGIN   : String = 'begin';
        static public const UPDATE  : String = 'update';
        static public const FINISH  : String = 'finish';

        public function TaskEvent ( type : String, bubbles : Boolean = false, cancelable : Boolean = false )
        {
            super ( type, bubbles, cancelable );
        }

        override public function clone () : Event
        {
            return new TaskEvent(type,bubbles,cancelable);
        }
    }
}
