/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 18:40
 */
package com.kroms.fribola.delegate
{
    import mx.rpc.AsyncToken;
    import mx.rpc.IResponder;
    import mx.rpc.Responder;
    import mx.rpc.events.FaultEvent;
    import mx.rpc.events.ResultEvent;
    import mx.rpc.remoting.RemoteObject;

    public class Delegate
    {
        protected var remote : RemoteObject;

        public function Delegate ( remote : RemoteObject )
        {
            this.remote = remote;
        }

        private function apply ( method : Function, parameters : Array = null, responder : IResponder = null ) : AsyncToken
        {
            var token : AsyncToken = method.apply( null, parameters );
            if ( responder )
                token.addResponder( responder );
            else
                token.addResponder( new Responder( onResult, onFault ) );

            return token;
        }

        protected function onResult ( e : ResultEvent ) : void
        {
            trace( "result !" + "\n" + e.result.toString() );
        }

        protected function onFault ( e : FaultEvent ) : void
        {
            trace( "fault !" + "\n" + e.fault.toString() );
        }
    }
}
