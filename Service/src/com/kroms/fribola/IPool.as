/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 02/03/2015
 * Time: 16:37
 */
package com.kroms.fribola
{

    import com.kroms.fribola.task.abstract.ITask;

    /**
     * interface to manage tasks in a pool-like style
     */
    public interface IPool
    {
        /**
         * either it directly executes the task (if no task already running), either it queues it for later execution
         * @param task the task to submit
         */
        function submit ( task : ITask ) : void;

        /**
         * queue the task in an array-like style
         * @param task the task to queue
         */
        function queue ( task : ITask ) : void;

        /**
         * execute the task
         * @param task the task to execute
         */
        function execute ( task : ITask ) : void;

        /**
         * make another attempt at executing the task
         * @param task the task to retry
         */
        function retry ( task : ITask ) : void;

        /**
         * clear all existing tasks from the pool
         */
        function clear () : void;

        /**
         * abort a currently executing task
         * @param task the task to abort
         */
        function abort ( task : ITask ) : void;

        /**
         * cancel an existing task, removing it from the pool
         * @param task the task to cancel
         */
        function cancel ( task : ITask ) : void;
    }
}
