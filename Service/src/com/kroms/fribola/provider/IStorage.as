/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 02/03/2015
 * Time: 16:46
 */
package com.kroms.fribola.provider
{
    /**
     * interface to handle storage creation and updates
     */
    public interface IStorage extends IProvider
    {
        /**
         * handle raw storage creation
         * e.g : a database creation sql script
         */
        function create () : void;

        /**
         * handle raw storage indexing
         * e.g : a database index creation sql script
         */
        function index () : void;

        /**
         * handle raw storage relationship creation
         * e.g : a database foreign keys relationship sql script
         */
        function join () : void;

        /**
         * upgrade a storage to a newer version
         * @param oldVersion the old version number
         * @param newVersion the new version number
         */
        function upgrade ( oldVersion : int, newVersion : int ) : void;

        /**
         * downgrade a storage to an older version
         * @param oldVersion the old version number
         * @param newVersion the new version number
         */
        function downgrade ( oldVersion : int, newVersion : int ) : void;

        /**
         * export storage content
         * @return the dumped content
         */
        function dump () : *;
    }
}
