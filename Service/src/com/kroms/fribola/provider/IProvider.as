/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 02/03/2015
 * Time: 16:13
 */
package com.kroms.fribola.provider
{

    import com.kroms.fribola.*;
    import com.kroms.fribola.vo.ValueObject;

    /**
     * interface for storage operations
     */
    public interface IProvider
    {
        function query ( projection : Vector.<String>, selection : String, selectionArgs : Vector.<String>, sortOrder : String ) : void;
        function select ( projection : Vector.<String>, selection : String, selectionArgs : Vector.<String>, sortOrder : String ) : void;
        function insert ( values : Vector.<ValueObject> ) : void;
        function update ( values : Vector.<ValueObject>, selection : String, selectionArgs : Vector.<String> ) : void;
        function remove ( keys : Vector.<IKey> ) : void;
    }
}
