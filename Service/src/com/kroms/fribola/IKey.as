/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 02/03/2015
 * Time: 16:33
 */
package com.kroms.fribola
{

    import com.kroms.fribola.database.IColumn;

    /**
     * interface for storage key (single key or multiple keys supported)
     * e.g : a database table primary key
     */
    public interface IKey
    {
        /**
         * the column(s) of the key
         */
        function get columns () : Vector.<IColumn>;
        function set columns ( value : Vector.<IColumn> ) : void;
    }
}
