/**
 * Created by Romain KELIFA on 10/10/2014.
 */
package com.kroms.fribola.conf
{
    import com.kroms.fribola.util.DescriptorUtil;

    import mx.resources.IResourceManager;
    import mx.resources.ResourceManager;

    public class DistriqtConf
	{
        static public const LICENCE_BUNDLE : String = 'licence';
		static private var _licence_key:String;

        /**
         * retrieve <b>Distriqt</b> required licence key for AIR Native Extensions from <code>licence</code> application resource bundle
         * the key must strictly be formatted like this :
         * for example if my application id is <code>com.company.application.MyApplication</code>, so my <code>licence</code> resource bundle key must be <code>com.company.application.MyApplication.DISTRIQT=some-key-obtained-on-Distriqt-ANE-purchase</code>
         * @return the content of <code>licence</code> resource bundle key, if it exists
         */
        static public function get LICENCE_KEY () : String
        {
            if ( !_licence_key )
            {
                var resource : IResourceManager = ResourceManager.getInstance();
                var id : String = DescriptorUtil.APPLICATION_ID;
                _licence_key = resource.getString( LICENCE_BUNDLE, id + "DISTRIQT" );
            }

            return _licence_key;
        }
	}
}
