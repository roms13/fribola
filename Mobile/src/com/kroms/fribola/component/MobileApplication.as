/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 16:25
 */
package com.kroms.fribola.component
{
    import com.kroms.fribola.impl.MobileApplicationImplementation;
    import com.kroms.fribola.impl.MobileJPEGEncoderImplementation;
    import com.kroms.fribola.impl.MobileNetworkInfoImplementation;

    public class MobileApplication extends CrossPlatformApplication
    {
        public function MobileApplication ()
        {
            super ();
        }

        override protected function initializeLibraries():void
        {
            super.initializeLibraries();

            Application = MobileApplicationImplementation.getInstance();
            JPEGEncoder = MobileJPEGEncoderImplementation.getInstance();
            NetworkInfo = MobileNetworkInfoImplementation.getInstance();

            Application.initialize();
            JPEGEncoder.initialize();
            NetworkInfo.initialize();
        }
    }
}
