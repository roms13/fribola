/**
 * Created by Romain KELIFA on 23/10/2014.
 */
package com.kroms.fribola.impl
{
    import com.distriqt.extension.networkinfo.NetworkInfo;
    import com.distriqt.extension.networkinfo.NetworkInterface;
    import com.distriqt.extension.networkinfo.events.NetworkInfoEvent;
    import com.kroms.fribola.conf.DistriqtConf;
    import com.kroms.fribola.util.StringUtil;

    public class MobileNetworkInfoImplementation extends DefaultNetworkInfoImplementable
    {
        static private var _instance : MobileNetworkInfoImplementation;

        override public function initialize():void
        {
            NetworkInfo.init( DistriqtConf.LICENCE_KEY );

            if ( NetworkInfo.isSupported )
            {
                NetworkInfo.networkInfo.addEventListener(NetworkInfoEvent.CHANGE, onNetworkChange);
            }
            else // fallback
            {
                super.initialize();
            }
        }

        override public function get addresses():Vector.<String>
        {
            var results : Vector.<String>;

            var infos_distriqt : Vector.<NetworkInterface>;
            var info_distriqt : NetworkInterface;

            var count : int;
            var i : int;

            var address : String;

            if ( NetworkInfo.isSupported )
            {
                results = new Vector.<String>();

                infos_distriqt = NetworkInfo.networkInfo.findInterfaces();
                count = infos_distriqt ? infos_distriqt.length : 0;

                for ( i = 0; i < count; i++ )
                {
                    info_distriqt = infos_distriqt[i];
                    address = info_distriqt.hardwareAddress;

                    if ( StringUtil.isNotEmpty( address ) && !contains( address, results ) )
                        results.push( address );
                }
            }
            else // fallback
            {
                results = super.addresses;
            }

            return results;
        }

        private function onNetworkChange( e:NetworkInfoEvent ):void
        {
            CONFIG::debug
            {
                trace( "Internet change. current status: "
                        + ( monitor.available ? 'connected' : 'disconnected' )
                );
            }

            setConnected( NetworkInfo.networkInfo.isReachable() );
            setMonitoring( true ); // attention à bien le laisser à la fin
        }

        static public function getInstance () : INetworkInfoImplementable
        {
            if ( !_instance )
                _instance = new MobileNetworkInfoImplementation();

            return _instance;
        }
    }
}
