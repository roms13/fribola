/**
 * Created by Romain KELIFA on 23/10/2014.
 */
package com.kroms.fribola.impl
{
	import com.distriqt.extension.application.Application;
	import com.kroms.fribola.conf.DistriqtConf;

	public class MobileApplicationImplementation implements IApplicationImplementable
    {
	    static private var _instance : MobileApplicationImplementation;

	    public function initialize () : void
	    {
	        Application.init( DistriqtConf.LICENCE_KEY );
	    }

	    public function get uid () : String
	    {
	        if ( Application.isSupported )
	            return Application.service.device.uniqueId();

	        // fallback method
	        var implementation : IApplicationImplementable = new DefaultApplicationImplementation();
	        return implementation.uid;
	    }

	    static public function getInstance () : IApplicationImplementable
	    {
	        if ( !_instance )
	            _instance = new MobileApplicationImplementation();

	        return _instance;
	    }
	}
}
