/**
 * Created by Romain KELIFA on 23/10/2014.
 */
package com.kroms.fribola.impl {
    import by.blooddy.crypto.image.JPEGEncoder;

    import cmodule.as3_jpeg_wrapper.CLibInit;

    import com.distriqt.extension.image.Image;
    import com.distriqt.extension.image.ImageFormat;
    import com.kroms.fribola.conf.DistriqtConf;
    import com.kroms.fribola.util.OSDetector;

    import flash.utils.ByteArray;

    public class MobileJPEGEncoderImplementation extends JPEGEncoderImplementation
{
    static private var _instance : MobileJPEGEncoderImplementation;

    override public function initialize():void
    {
        if ( !OSDetector.isIOS )
        {
            alchemy = new CLibInit;
            library = alchemy.init();
        }
        else
        {
            Image.init( DistriqtConf.LICENCE_KEY );
        }
    }

    override public function encode(source:*, width:Number, height:Number):ByteArray
    {
        if ( !OSDetector.isIOS )
        {
            return library.write_jpeg_file(source, width, height, 3, 2, encodingQuality);
        }
        else if ( Image.isSupported )
        {
            var bytes : ByteArray = new ByteArray();
            var success : Boolean = Image.service.encode( source, bytes, ImageFormat.JPG, ( encodingQuality * .1 ) );

            if ( success )
                return bytes;
        }

        return JPEGEncoder.encode(source, encodingQuality);
    }

    static public function getInstance () : IJPEGEncoderImplementable
    {
        if ( !_instance )
            _instance = new MobileJPEGEncoderImplementation();

        return _instance;
    }
}
}
