/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 20/12/2015
 * Time: 18:31
 */
package com.kroms.fribola.manager
{
    import com.kroms.fribola.event.StageWebViewManagerEvent;

    import flash.display.Stage;
    import flash.display.StageAlign;
    import flash.display.StageScaleMode;
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.geom.Rectangle;
    import flash.media.StageWebView;

    import mx.core.RuntimeDPIProvider;
    import mx.core.mx_internal;

    use namespace mx_internal;

    public class StageWebViewManager extends EventDispatcher
    {
        private var _stageWebView                   : StageWebView;

        public function create ( stage : Stage ) : void
        {
            if ( !stage )
                throw new Error("missing reference to Stage");

            _stageWebView = new StageWebView();
            stage.scaleMode = StageScaleMode.NO_SCALE;
            stage.align = StageAlign.TOP_LEFT;
            _stageWebView.stage = stage;

            var bounds : Rectangle = stage.getBounds(stage);

            var w : int = stage.stageWidth;
            var isRetina : Boolean = ( w > RuntimeDPIProvider.IPAD_MAX_EXTENT );

            var horizontal_offset : int = Math.abs( bounds.x );
            var vertical_offset : Number = Math.abs( bounds.y );
            if ( isRetina ) vertical_offset = vertical_offset * 2;

            var h : int = stage.stageHeight - vertical_offset;

            _stageWebView.viewPort = new Rectangle( horizontal_offset, vertical_offset, w, h);
            _stageWebView.addEventListener( Event.COMPLETE, onComplete );

            dispatchEvent( new StageWebViewManagerEvent( StageWebViewManagerEvent.CREATED ) );
        }

        public function dispose () : void
        {
            if ( _stageWebView.hasEventListener( Event.COMPLETE ) )
                _stageWebView.removeEventListener( Event.COMPLETE, onComplete );

            _stageWebView.viewPort = null;
            _stageWebView.dispose();
            _stageWebView = null;

            dispatchEvent( new StageWebViewManagerEvent( StageWebViewManagerEvent.DISPOSED ) );
        }

        public function load ( url : String ) : void
        {
            if ( !_stageWebView )
                throw new Error("StageWebView must be created prior to attempt to load anything inside");

            if ( !url || url == '' )
                throw new Error("StageWebView must be provided with a substantial url");

            _stageWebView.loadURL( url );
        }

        private function onComplete ( e : Event ) : void
        {
            dispatchEvent( new StageWebViewManagerEvent(StageWebViewManagerEvent.LOADED) );
        }
    }
}
