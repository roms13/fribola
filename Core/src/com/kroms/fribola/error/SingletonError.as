/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 03/12/2015
 * Time: 21:30
 */
package com.kroms.fribola.error
{

    import mx.resources.IResourceManager;
    import mx.resources.ResourceManager;

    public class SingletonError extends Error
    {
        protected var resourceManager : IResourceManager = ResourceManager.getInstance();

        public function SingletonError ( message : * = "", id : * = 0 )
        {
            super ( message + " : " + resourceManager.getString('fribola','core.error.singleton'), id );
        }
    }
}
