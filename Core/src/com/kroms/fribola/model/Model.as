/**
 * Created by Romain KELIFA on 31/10/2014.
 */
package com.kroms.fribola.model
{
    import avmplus.getQualifiedClassName;

    import com.kroms.fribola.error.SingletonError;

    public class Model implements IModel
    {
        static private var _instance : Model;
        static private const ENFORCER : Object = {};

        private var _departments : Array;
        private var _cities : Array;

        public function Model ( enforcer : Object )
        {
            super();

            if ( !enforcer || enforcer != ENFORCER )
                throw new SingletonError( getQualifiedClassName(this) );
        }

        static public function getInstance () : Model
        {
            if ( !_instance )
                _instance = new Model( ENFORCER );

            return _instance;
        }

        public function get departments () : Array { return _departments; }
        public function set departments ( value : Array ) : void
        {
            _departments = value;
        }

        public function get cities () : Array { return _cities; }
        public function set cities ( value : Array ) : void
        {
            _cities = value;
        }
    }
}
