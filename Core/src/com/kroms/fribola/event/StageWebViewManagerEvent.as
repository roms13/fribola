/**
 * Created by Romain KELIFA on 07/10/2014.
 */
package com.kroms.fribola.event
{
    import flash.events.Event;

    public class StageWebViewManagerEvent extends Event
    {
        static public const CREATED     : String = "created";
        static public const LOADED      : String = "loaded";
        static public const DISPOSED    : String = "disposed";

        public function StageWebViewManagerEvent( type:String, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
        }

        override public function clone() : Event
        {
            return new StageWebViewManagerEvent(type,bubbles,cancelable);
        }
    }
}
