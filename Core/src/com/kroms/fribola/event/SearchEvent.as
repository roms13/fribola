/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 14/12/2015
 * Time: 12:07
 */
package com.kroms.fribola.event
{
    import flash.events.Event;

    public class SearchEvent extends Event
    {
        static public const KEYWORD : String = "keyword";
        static public const FILTER : String = "filter";
        static public const SORT : String = "sort";

        public function SearchEvent ( type : String, bubbles : Boolean = false, cancelable : Boolean = false )
        {
            super ( type, bubbles, cancelable );
        }

        override public function clone () : Event
        {
            return new SearchEvent(type,bubbles,cancelable);
        }
    }
}
