/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 21:10
 */
package com.kroms.fribola.event
{
    import flash.events.Event;

    public class ChoiceEvent extends Event
    {
        static public const CONFIRM : String = 'confirm';
        static public const CANCEL  : String = 'cancel';
        static public const ABORT   : String = 'abort';

        public function ChoiceEvent ( type : String, bubbles : Boolean = false, cancelable : Boolean = false )
        {
            super ( type, bubbles, cancelable );
        }

        override public function clone () : Event
        {
            return new ChoiceEvent( type, bubbles, cancelable );
        }
    }
}
