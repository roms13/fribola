/**
 * Created by Romain KELIFA on 23/12/2014.
 */
package com.kroms.fribola.event
{
    import flash.events.Event;

    public class FormEvent extends Event
    {
        static public const SUBMIT : String = "submit";
        static public const CANCEL : String = "cancel";
        static public const RESET : String = "reset";

        static public const ADD : String = "add";
        static public const EDIT : String = "edit";

        static public const CREATE : String = "create"; // TODO : refactor these as committing events (with embedded data) ?
        static public const UPDATE : String = "update";
        static public const DELETE : String = "delete";
        static public const DEACTIVATE : String = "deactivate";
        static public const ACTIVATE : String = "activate";

        public function FormEvent ( type : String, bubbles : Boolean = false, cancelable : Boolean = false )
        {
            super ( type, bubbles, cancelable );
        }

        override public function clone () : Event
        {
            return new FormEvent( type, bubbles, cancelable );
        }
    }
}
