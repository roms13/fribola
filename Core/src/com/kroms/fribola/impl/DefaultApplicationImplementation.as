/**
 * Created by Romain KELIFA on 23/10/2014.
 */
package com.kroms.fribola.impl
{
    import flash.system.Capabilities;

    public class DefaultApplicationImplementation implements IApplicationImplementable
    {
        static private var _instance : DefaultApplicationImplementation;

        // no need to initialize anything for desktop implementation
        public function initialize () : void
        {}

        public function get uid () : String
        {
            /*
             trace( "Capabilities manufacturer: " + Capabilities.manufacturer ); // no because it's based on AIR manufacturer version
             trace( "Capabilities os: " + Capabilities.os );
             trace( "Capabilities cpu architecture: " + Capabilities.cpuArchitecture );
             trace( "Capabilities pixel aspect ratio: " + Capabilities.pixelAspectRatio ); // weird
             trace( "Capabilities player type: " + Capabilities.playerType );
             trace( "Capabilities version: " + Capabilities.version ); // no because it's based on AIR version (likely to change over time)
             trace( "Capabilities screenResolutionX: " + Capabilities.screenResolutionX ); // no because it's based on screen (likely to change over time)
             trace( "Capabilities screenResolutionY: " + Capabilities.screenResolutionY ); // no because it's based on screen (likely to change over time)
             */

            var capabilities : String = [ Capabilities.os, Capabilities.cpuArchitecture, Capabilities.playerType ].join("&");
            return capabilities;
        }

        static public function getInstance () : DefaultApplicationImplementation
        {
            if ( !_instance )
                _instance = new DefaultApplicationImplementation();

            return _instance;
        }
    }
}
