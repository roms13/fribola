/**
 * Created by Romain KELIFA on 23/10/2014.
 */
package com.kroms.fribola.impl
{
	import air.net.URLMonitor;

	import com.kroms.fribola.util.StringUtil;

	import flash.events.StatusEvent;
	import flash.net.NetworkInfo;
	import flash.net.NetworkInterface;
	import flash.net.URLRequest;

	public class DefaultNetworkInfoImplementable extends NetworkInfoImplementable
	{
	    static private var _instance : DefaultNetworkInfoImplementable;

		protected var monitor:URLMonitor;

	    // no need to initialize anything for desktop implementation
	    override public function initialize():void
	    {
			if ( NetworkInfo.isSupported )
            {
				var request:URLRequest = new URLRequest( CONFIG::ping );
				request.idleTimeout = 10000;
			    monitor = new URLMonitor(request);
			    monitor.addEventListener(StatusEvent.STATUS, onStatus);
			    monitor.start();
		    }
	    }

	    override public function get addresses():Vector.<String>
	    {
	        var results : Vector.<String>;

	        var infos_core : Vector.<NetworkInterface>;
	        var info_core : NetworkInterface;

	        var count : int;
	        var i : int;

	        var address : String;

	        if ( NetworkInfo.isSupported )
	        {
	            results = new Vector.<String>();

	            infos_core = NetworkInfo.networkInfo.findInterfaces();
	            count = infos_core ? infos_core.length : 0;

	            for ( i = 0; i < count; i++ )
	            {
	                info_core = infos_core[i];
	                address = info_core.hardwareAddress;

	                if ( StringUtil.isNotEmpty( address ) && !contains( address, results ) )
	                    results.push( address );
	            }
	        }

	        return results;
	    }

		private function onStatus(e:StatusEvent):void
        {
			CONFIG::debug
			{
				trace( "Internet change. current status: "
						+ ( monitor.available ? 'connected' : 'disconnected' )
				);
			}

			setConnected( monitor.available );
			setMonitoring(true); // monitoring must be dispatched at the end
		}

	    static public function getInstance () : INetworkInfoImplementable
	    {
	        if ( !_instance )
	            _instance = new DefaultNetworkInfoImplementable();

	        return _instance;
	    }
	}
}
