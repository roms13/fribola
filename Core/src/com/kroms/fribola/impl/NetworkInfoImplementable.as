/**
 * Created by Romain KELIFA on 23/10/2014.
 */
package com.kroms.fribola.impl
{
    import com.kroms.fribola.util.StringUtil;

    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.StatusEvent;

    [Event(name="status",type="flash.events.StatusEvent")]
    [Event(name="monitoring",type="flash.events.Event")]
    public class NetworkInfoImplementable extends EventDispatcher implements INetworkInfoImplementable
    {
        static public const MONITORING : String = "monitoring";

        private var _connected  : Boolean;
        private var _monitoring : Boolean;

        public function initialize():void
        {
            throw new Error("this class is not meant to be instantiated");
        }

        public function get addresses():Vector.<String>
        {
            return null;
        }

        protected function contains ( item : String, vector : Vector.<String> ) : Boolean
        {
            if ( StringUtil.isEmpty( item ) )
                return false;

            var count : int = vector ? vector.length : 0;
            var existing : String;

            for ( var i : int  = 0; i < count; i++ )
            {
                existing = vector[i];

                if ( existing == item )
                    return true;
            }

            return false;
        }

        public function get monitoring () : Boolean { return _monitoring; }
        protected function setMonitoring ( value : Boolean ) : void
        {
            if ( _monitoring == value )
                return;

            _monitoring = value;
            dispatchEvent( new Event( MONITORING ) );
        }

        public function get connected () : Boolean { return _connected; }
        protected function setConnected ( value : Boolean ) : void
        {
            if ( _connected == value )
                return;

            _connected = value;
            dispatchEvent( new StatusEvent( StatusEvent.STATUS ) );
        }
    }
}
