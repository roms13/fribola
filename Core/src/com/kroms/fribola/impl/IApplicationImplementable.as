/**
 * Created by Romain KELIFA on 23/10/2014.
 */
package com.kroms.fribola.impl
{
    public interface IApplicationImplementable extends IImplementable
    {
        function get uid () : String;
    }
}
