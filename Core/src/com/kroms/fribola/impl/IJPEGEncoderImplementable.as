/**
 * Created by Romain KELIFA on 23/10/2014.
 */
package com.kroms.fribola.impl
{
    import flash.utils.ByteArray;

    public interface IJPEGEncoderImplementable extends IImplementable
    {
        function encode ( source : *, width : Number, height : Number ) : ByteArray;
        function get quality () : uint;
        function set quality ( value : uint ) : void;
    }
}
