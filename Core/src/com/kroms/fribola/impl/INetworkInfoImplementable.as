/**
 * Created by Romain KELIFA on 23/10/2014.
 */
package com.kroms.fribola.impl
{
    import flash.events.IEventDispatcher;

    public interface INetworkInfoImplementable extends IImplementable, IEventDispatcher
    {
        function get addresses () : Vector.<String>;
        function get connected () : Boolean;
        function get monitoring () : Boolean;
    }
}
