/**
 * Created by Romain KELIFA on 23/10/2014.
 */
package com.kroms.fribola.impl
{
    import flash.utils.ByteArray;

    public class JPEGEncoderImplementation implements IJPEGEncoderImplementable
    {
        protected var encodingQuality : uint = 70;
        protected var alchemy : Object;
        protected var library : Object;

        public function initialize():void
        {
            throw new Error("this class is not meant to be instantiated");
        }

        public function encode(source:*, width:Number, height:Number):ByteArray
        {
            return null;
        }

        public function get quality () : uint { return encodingQuality; }
        public function set quality ( value : uint ) : void
        {
            if ( encodingQuality != value )
            {
                encodingQuality = value;
            }
        }
    }
}
