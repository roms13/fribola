/**
 * Created by Romain KELIFA on 23/10/2014.
 */
package com.kroms.fribola.impl
{
    import by.blooddy.crypto.image.JPEGEncoder;

    import cmodule.as3_jpeg_wrapper.CLibInit;

    import com.kroms.fribola.util.OSDetector;

    import flash.utils.ByteArray;

    public class DefaultJPEGEncoderImplementation extends JPEGEncoderImplementation
    {
        static private var _instance : DefaultJPEGEncoderImplementation;
        private var isSupported : Boolean = true;

        override public function initialize () : void
        {
            if ( !OSDetector.isIOS )
            {
                try
                {
                    alchemy = new CLibInit;
                    library = alchemy.init();
                }
                catch(e:Error)
                {
                    isSupported = false;
                }
            }
        }

        override public function encode ( source : *, width : Number, height : Number ) : ByteArray
        {
            if ( !OSDetector.isIOS && isSupported )
            //if ( !OSDetector.isIOS && isSupported )
            {
                return library.write_jpeg_file(source, width, height, 3, 2, encodingQuality);
            }

            return JPEGEncoder.encode(source, encodingQuality);
        }

        static public function getInstance () : IJPEGEncoderImplementable
        {
            if ( !_instance )
                _instance = new DefaultJPEGEncoderImplementation();

            return _instance;
        }
    }
}
