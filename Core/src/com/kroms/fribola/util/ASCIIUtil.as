/**
 * Created by Romain KELIFA on 04/01/2015.
 */
package com.kroms.fribola.util
{
    public class ASCIIUtil
    {
        static public const REGEXP_APOSTROPHE               : RegExp = RegexpUtil.global('#39');
        static public const REGEXP_AMPERSAND                : RegExp = RegexpUtil.global('#38');

        static public const REGEXP_A_GRAVE                  : RegExp = RegexpUtil.global('#224');
        static public const REGEXP_A_CIRCUMFLEX             : RegExp = RegexpUtil.global('#226');
        static public const REGEXP_A_DIARESIS               : RegExp = RegexpUtil.global('#228');

        static public const REGEXP_C_CEDILLA                : RegExp = RegexpUtil.global('#231');

        static public const REGEXP_E_GRAVE                  : RegExp = RegexpUtil.global('#232');
        static public const REGEXP_E_ACUTE                  : RegExp = RegexpUtil.global('#233');
        static public const REGEXP_E_CIRCUMFLEX             : RegExp = RegexpUtil.global('#234');
        static public const REGEXP_E_DIARESIS               : RegExp = RegexpUtil.global('#235');

        static public const REGEXP_I_CIRCUMFLEX             : RegExp = RegexpUtil.global('#238');
        static public const REGEXP_I_DIARESIS               : RegExp = RegexpUtil.global('#239');

        static public const REGEXP_O_CIRCUMFLEX             : RegExp = RegexpUtil.global('#244');
        static public const REGEXP_O_DIARESIS               : RegExp = RegexpUtil.global('#246');

        static public const REGEXP_U_GRAVE                  : RegExp = RegexpUtil.global('#249');
        static public const REGEXP_U_CIRCUMFLEX             : RegExp = RegexpUtil.global('#251');
        static public const REGEXP_U_DIARESIS               : RegExp = RegexpUtil.global('#252');

        static public const REGEXP_A_GRAVE_UPPER            : RegExp = RegexpUtil.global('#192');
        static public const REGEXP_A_CIRCUMFLEX_UPPER       : RegExp = RegexpUtil.global('#194');
        static public const REGEXP_A_DIARESIS_UPPER         : RegExp = RegexpUtil.global('#196');

        static public const REGEXP_C_CEDILLA_UPPER          : RegExp = RegexpUtil.global('#199');

        static public const REGEXP_E_GRAVE_UPPER            : RegExp = RegexpUtil.global('#200');
        static public const REGEXP_E_ACUTE_UPPER            : RegExp = RegexpUtil.global('#201');
        static public const REGEXP_E_CIRCUMFLEX_UPPER       : RegExp = RegexpUtil.global('#202');
        static public const REGEXP_E_DIARESIS_UPPER         : RegExp = RegexpUtil.global('#203');

        static public const REGEXP_I_CIRCUMFLEX_UPPER       : RegExp = RegexpUtil.global('#206');
        static public const REGEXP_I_DIARESIS_UPPER         : RegExp = RegexpUtil.global('#207');

        static public const REGEXP_O_CIRCUMFLEX_UPPER       : RegExp = RegexpUtil.global('#212');
        static public const REGEXP_O_DIARESIS_UPPER         : RegExp = RegexpUtil.global('#214');

        static public const REGEXP_U_CIRCUMFLEX_UPPER       : RegExp = RegexpUtil.global('#217');
        static public const REGEXP_U_GRAVE_UPPER            : RegExp = RegexpUtil.global('#219');
        static public const REGEXP_U_DIARESIS_UPPER         : RegExp = RegexpUtil.global('#220');

        static public const REGEXP_EURO                     : RegExp = RegexpUtil.global('#8364');
        static public const REGEXP_EURO_BIS                 : RegExp = RegexpUtil.global('¬');
    }
}
