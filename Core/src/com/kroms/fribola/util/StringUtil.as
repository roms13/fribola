/**
 * Created by Romain KELIFA on 30/10/2014.
 */
package com.kroms.fribola.util
{
    import com.adobe.utils.StringUtil;

    public class StringUtil
    {
        static public function isEmpty ( value : String ) : Boolean
        {
            return ( !value || StringUtil.trim( value ) == '' );
        }

        static public function isNotEmpty ( value : String ) : Boolean
        {
            return ( value && StringUtil.trim( value ) != '' );
        }

        static public function startsWith ( sentence : String, words : String, caseInsensitive : Boolean = false, accentInsensitive : Boolean = false ) : Boolean
        {
            if ( isEmpty( sentence ) || isEmpty( words ) )
                return false;

            var source : String = caseInsensitive ? sentence.toLowerCase() : sentence;
            var contained : String = caseInsensitive ? words.toLowerCase() : words;

            if ( accentInsensitive )
            {
                source = stripAccent( source );
                contained = stripAccent( contained );
            }

            return ( source.indexOf( contained ) == 0 );
        }

        static public function stripWordsShorterThan( words : Array, minimum_length : uint ) : Array
        {
            if ( !words )
                return null;

            var clean : Array = words.slice();
            var word : String;
            var count : int = clean ? clean.length : 0;

            for ( var i : int = ( count -1 ); i >= 0; i-- )
            {
                word = clean[i];

                if ( !word || word.length < minimum_length )
                    clean.splice( i, 1 );
            }

            return clean;
        }

        static public function contains ( sentence : String, words : String,
                                          caseInsensitive : Boolean = false,
                                          accentInsensitive : Boolean = false ) : Boolean
        {
            if ( isEmpty( sentence ) || isEmpty( words ) )
                return false;

            var source : String = caseInsensitive ? sentence.toLowerCase() : sentence;
            var contained : String = caseInsensitive ? words.toLowerCase() : words;

            if ( accentInsensitive )
            {
                source = stripAccent( source );
                contained = stripAccent( contained );
            }

            return ( source.indexOf( contained ) != -1 );
        }

        static public function trim( string : String ) : String
        {
            return com.adobe.utils.StringUtil.trim( string );
        }

        /**
         * Remove all accents from a source string.
         * Beware as it's time processing if applied to large data sets.
         *
         * all credits to Laurent Deketelaere
         * http://blog.geturl.net/post/2010/01/31/%5BAS3%5D-Supprimer-tous-les-accents-d-une-chaine-de-caract%C3%A8re
         */
        public static function stripAccent(source : String) : String
        {
            source = source.replace(/[āăąàáâãäå]/g, "a");
            source = source.replace(/[ĀĂĄÀÁÂÃÄÅ]/g, "A");
            source = source.replace(/[ƀƃƅ]/g, "b");
            source = source.replace(/[ƁƂƄ]/g, "B");
            source = source.replace(/[ç¢ćĉċčƈ]/g, "c");
            source = source.replace(/[ÇĆĈĊČƇ]/g, "C");
            source = source.replace(/[ďđƌ]/g, "d");
            source = source.replace(/[ĎĐƉƊƋ]/g, "D");
            source = source.replace(/[èéêëēĕėęě]/g, "e");
            source = source.replace(/[ËÉÊÈĒĔĖĘĚ]/g, "E");
            source = source.replace(/[ĝğġģ]/g, "g");
            source = source.replace(/[ĜĞĠĢ]/g, "G");
            source = source.replace(/[ĥħ]/g, "h");
            source = source.replace(/[ĤĦ]/g, "H");
            source = source.replace(/[ìíîïĩīĭįı]/g, "i");
            source = source.replace(/[ÌÍÎÏĨĪĬĮİ]/g, "I");
            source = source.replace(/[ĵ]/g, "J");
            source = source.replace(/[Ĵ]/g, "J");
            source = source.replace(/[ķĸ]/g, "k");
            source = source.replace(/[Ķ]/g, "K");
            source = source.replace(/[ĺļľŀł]/g, "l");
            source = source.replace(/[ĹĻĽĿŁ]/g, "L");
            source = source.replace(/[ñńņňŉ]/g, "n");
            source = source.replace(/[ÑŃŅŇ]/g, "N");
            source = source.replace(/[ðòóôõöøōŏő]/g, "o");
            source = source.replace(/[ÒÓÔÕÖØŌŎŐƆ]/g, "O");
            source = source.replace(/[ŕŗř]/g, "r");
            source = source.replace(/[ŔŖŘ]/g, "R");
            source = source.replace(/[šśŝş]/g, "s");
            source = source.replace(/[ŠŚŜŞ]/g, "S");
            source = source.replace(/[ţťŧ]/g, "t");
            source = source.replace(/[ŢŤŦ]/g, "T");
            source = source.replace(/[ùúûüũūŭůűų]/g, "u");
            source = source.replace(/[ÙÚÛÜŨŪŬŮŰŲ]/g, "U");
            source = source.replace(/[ŵ]/g, "w");
            source = source.replace(/[Ŵ]/g, "W");
            source = source.replace(/[ýÿŷ]/g, "y");
            source = source.replace(/[ÝŸŶ]/g, "Y");
            source = source.replace(/[žźż]/g, "z");
            source = source.replace(/[ŽŹŻ]/g, "Z");
            source = source.replace(/[æ]/g, "ae");
            source = source.replace(/[Æ]/g, "AE");
            source = source.replace(/[œ]/g, "oe");
            source = source.replace(/[Œ]/g, "OE");
            return source;
        }

        static public function formatAS3ClassName ( qualifiedClassName : String ) : String
        {
            if ( !qualifiedClassName )
                return null;

            return qualifiedClassName.replace('::','.');
        }
    }
}
