/**
 * Created by Romain KELIFA on 04/01/2015.
 */
package com.kroms.fribola.util
{
    import com.kroms.fribola.conf.LocationConf;
    import com.kroms.fribola.data.core.City;
    import com.kroms.fribola.data.core.Department;
    import com.kroms.fribola.model.Model;

    /*
     * utility class to handle french department, city and zipcodes matters
     * WIKIPEDIA : http://fr.wikipedia.org/wiki/Code_postal_en_France : rubrique Composition
     */
    public class CityUtil
    {
        static private var model : Model = Model.getInstance();

        static private var fast_collection : Vector.<Department>;
        static public function getDepartmentByCode ( code : String, refresh : Boolean = false ) : Department
        {
            var no_fast_collection : Boolean = !fast_collection;
            var count : int;
            var i : int;

            // creates a faster copy once, as departments list is retrieved only once
            // but you can also refresh if required later on
            if ( no_fast_collection || refresh )
            {
                var collection : Array = model ? model.departments : null;
                count = collection ? collection.length : 0;

                if ( count > 0 )
                {
                    fast_collection = new <Department>[];

                    for ( i = 0; i < count; i++ )
                    {
                        fast_collection.push( collection[i] as Department );
                    }

                    count = fast_collection ? fast_collection.length : 0;
                }
            }

            var department : Department;
            var department_code : String;

            for ( i = 0; i < count; i++ )
            {
                department = fast_collection[i] as Department;
                department_code = department ? department.code : null;

                if ( department_code == code )
                    return department;
            }

            return null;
        }

        static public function getCitiesFasterCopy () : Vector.<City>
        {
            var collection : Array = model ? model.cities : null;
            var count : int = collection ? collection.length : 0;

            if ( count < 1 )
                return null;

            var copy : Vector.<City> = new <City>[];

            for ( var i : int = 0; i < count; i++ )
            {
                copy.push( collection[i] as City );
            }

            return copy;
        }

        static public function getDepartmentCodeByCity ( city : City ) : String
        {
            if ( !city )
                return null;

            var collection : Vector.<City> = getCitiesFasterCopy();
            var count : int = collection ? collection.length : 0;
            var item : City;
            var sameDenomination : Boolean;
            var sameZipcode : Boolean;

            for ( var i : int = 0; i < count; i++ )
            {
                item = collection[i];
                sameDenomination = city.denomination == item.denomination;
                sameZipcode = city.zipcode == item.zipcode;

                if ( sameDenomination && sameZipcode )
                    return item.department_uid;
            }

            return null;
        }

        static public function getCityFromDetails ( details : String ) : City
        {
            var empty : Boolean = StringUtil.isEmpty( details );

            if ( empty )
                return null;

            var collection : Vector.<City> = getCitiesFasterCopy();
            var count : int = collection ? collection.length : 0;
            var item : City;

            for ( var i : int; i < count; i++ )
            {
                item = collection[i];

                if ( StringUtil.contains( details, item.denomination, true, true ) )
                    return item;
            }

            return null;
        }

        static public function getCityExtra ( city : City, details : String ) : String
        {
            if ( !city || StringUtil.isEmpty( details ) )
                return null;

            var denomination_cleaned : String = StringUtil.stripAccent( city.denomination );
            var details_cleaned : String = StringUtil.stripAccent( details );
            var extra : String;

            if ( details_cleaned.indexOf( denomination_cleaned ) == 0 )
            {
                var length : int = denomination_cleaned.length;
                extra = details.substr(length,details.length);
            }

            return extra;
        }

        static public function getDetails ( city : City, extra : String ) : String
        {
            var denomination : String = city && StringUtil.isNotEmpty( city.denomination ) ? city.denomination : null;

            if ( !denomination )
                return null;

            var details : String = denomination;

            if ( StringUtil.isNotEmpty( extra ) )
                details += ' ' + StringUtil.trim( extra );

            return details;
        }

        static public function getCityByUID ( value : String ) : City
        {
            if ( StringUtil.isEmpty( value ) )
                return null;

            var collection : Vector.<City> = getCitiesFasterCopy();
            var count : int = collection ? collection.length : 0;
            var item : City;

            for ( var i : int = 0; i < count; i++ )
            {
                item = collection[i];
                if ( item.uid == value )
                    return item;
            }

            return null;
        }

        static public function validZipcode ( zipcode : String, city : City ) : Boolean
        {
            if ( StringUtil.isEmpty( zipcode ) || !city )
                return false;

            var multiple : Boolean = hasMultipleZipcodes( city );
            if ( !multiple )
            {
                var same : Boolean = ( city.zipcode == zipcode );
                if ( same )
                    return true;

                var sameFirst3Letters : Boolean = compared.indexOf( zipcode.substr( 0, 3 ) ) == 0;
                return sameFirst3Letters;
            }

            var zipcodes : Array = city.zipcode.split( LocationConf.CITY_ZIPCODE_DELIMITER );
            var count : int = zipcodes ? zipcodes.length : 0;
            var compared : String;

            for ( var i : int = 0; i < count; i++ )
            {
                compared = zipcodes[i];
                if ( compared == zipcode )
                    return true;
            }

            return false;
        }

        static public function hasMultipleZipcodes ( city : City ) : Boolean
        {
            if ( !city )
                return false;

            var zipcodes : Array = city.zipcode.split( LocationConf.CITY_ZIPCODE_DELIMITER );
            var count : int = zipcodes ? zipcodes.length : 0;
            return ( count > 1 );
        }

        static public function getFirstZipcodeInCity ( city : City ) : int
        {
            if ( !city )
                return -1;

            var zipcodes : Array = city.zipcode.split( LocationConf.CITY_ZIPCODE_DELIMITER );
            var count : int = zipcodes ? zipcodes.length : 0;
            var exists : Boolean = count > 0;

            if ( exists )
            {
                var single : Boolean = count == 1;
                var first : uint = zipcodes[0];

                if ( single )
                {
                    return first;
                }
                else // multiple zipcodes
                {
                    return getFirstZipcodeInRange ( zipcodes );
                }
            }

            return -1;
        }

        static public function getLastZipcodeInCity ( city : City ) : int
        {
            if ( !city )
                return -1;

            var zipcodes : Array = city.zipcode.split( LocationConf.CITY_ZIPCODE_DELIMITER );
            var count : int = zipcodes ? zipcodes.length : 0;
            var exists : Boolean = count > 0;

            if ( exists )
            {
                var single : Boolean = count == 1;
                var first : uint = zipcodes[0];

                if ( single )
                {
                    return isPrefecture( first ) ? getPotentialLastZipcode( first ) : first;
                }
                else // multiple zipcodes
                {
                    return getLastZipcodeInRange ( zipcodes );
                }
            }

            return -1;
        }

        static public function getPotentialLastZipcode ( zipcode : int ) : int
        {
            if ( DigitUtil.isEmpty( zipcode ) )
                return -1;

            var current : String = zipcode.toString(); // let's say '73000' / '73100' / '73110' / '73048'
            var length : int = current.length;
            var last : String = current.substr( ( length - 1 ), 1 );
            var penultimate : String = current.substr( ( length - 2 ), 1 );
            var potential : String;

            if ( last == '0' )
                potential = '9';
            else
                return zipcode;

            if ( penultimate == '0' )
                potential = '99';

            potential = current.substr( 0, ( length - potential.length ) ) + potential;// will give '73099' / '73199' / '73119' / '73048'

            return parseInt( potential );
        }

        static public function getFirstZipcodeInRange ( zipcodes : Array ) : int
        {
            var first : int = -1;
            var count : int = zipcodes ? zipcodes.length : 0;
            var zipcode : int;

            for ( var i : int; i < count; i++ )
            {
                zipcode = zipcodes[i];

                if ( i > 0 )
                {
                    if ( first > zipcode )
                        first = zipcode;
                }
                else
                {
                    first = zipcode;
                }
            }

            return first;
        }

        static public function getLastZipcodeInRange ( zipcodes : Array ) : int
        {
            var last : int = -1;
            var count : int = zipcodes ? zipcodes.length : 0;
            var zipcode : int;

            for ( var i : int; i < count; i++ )
            {
                zipcode = zipcodes[i];

                if ( i > 0 )
                {
                    if ( last < zipcode )
                        last = zipcode;
                }
                else
                {
                    last = zipcode;
                }
            }

            if ( isPrefecture( last ) )
                last = getPotentialLastZipcode( last );

            return last;
        }

        static public function isPrefecture ( zipcode : int ) : Boolean
        {
            if ( DigitUtil.isEmpty( zipcode ) )
                return false;

            var z : String = zipcode.toString();
            var length : int = z.length;
            var part : String = z.substr( length -1, 1 );
            return ( part == '0' );
        }

        static public function filter_by_keywords ( item : Object, words : String ) : Boolean
        {
            var not_null : Boolean = ( item != null );
            var has_keywords : Boolean = words && StringUtil.isNotEmpty( words );

            var ok : Boolean = ( not_null || has_keywords );

            if ( !ok )
                return false;

            var city : City = item as City;

            var keywords : Array = words.split( ' ' );
            var count : int = keywords ? keywords.length : 0;
            var keyword : String;

            var has_keyword_denomination : Boolean;
            var has_keyword_department_denomination : Boolean;
            var has_keyword_zipcode : Boolean;

            var has_department_uid : Boolean;
            var department : Department;
            var department_denomination : String;

            var found : Boolean;
            var is_keyword_digit : Boolean;

            for ( var i : int = 0; i < count; i++ )
            {
                keyword = keywords[i];

                is_keyword_digit = DigitUtil.isDigit( keyword );

                if ( !is_keyword_digit ) keyword = keyword.toLowerCase();

                has_keyword_denomination = !is_keyword_digit && StringUtil.startsWith( city.denomination, keyword, true );
                has_keyword_zipcode = is_keyword_digit && containsZipcode( city.zipcode, DigitUtil.zeroFilled( keyword, LocationConf.FRENCH_ZIPCODE_LENGTH ) );

                found = ( has_keyword_denomination || has_keyword_zipcode );

                // only search for department denomination if not found before as it's way slower
                // you cannot start search from, let's say kewyords with 3-chars length or more, because some cities have denomination of one or 2 letters only
                // to improve performance, be certain to add a kind of delay between each call to filter (for example every 400 ms)
                if ( !found && !is_keyword_digit )
                {
                    has_department_uid = StringUtil.isNotEmpty( city.department_uid );
                    department = has_department_uid ? getDepartmentByCode( city.department_uid ) : null;
                    department_denomination = department && StringUtil.isNotEmpty( department.denomination ) ? department.denomination.toLowerCase() : null;
                    has_keyword_department_denomination = department_denomination ? department_denomination.indexOf( keyword ) != -1 : false;
                    found = has_keyword_department_denomination;
                }

                if ( !found )
                    return false;
            }

            return found;
        }

        static public function containsZipcode ( zipcodes : String, zipcode : String ) : Boolean
        {
            if ( StringUtil.isEmpty( zipcodes ) || StringUtil.isEmpty( zipcode ) )
                return false;

            var multiple : Array = zipcodes.split( LocationConf.CITY_ZIPCODE_DELIMITER );
            var count : int = multiple ? multiple.length : 0;

            if ( count > 1 )
            {
                for ( var i : int = 0; i < count; i++ )
                {
                    if ( multiple[i] == zipcode )
                        return true;
                }

                return false;
            }

            return ( zipcodes == zipcode );
        }
    }
}
