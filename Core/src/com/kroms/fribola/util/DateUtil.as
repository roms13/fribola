/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 21:44
 */
package com.kroms.fribola.util
{
    import org.as3commons.lang.DateUtils;

    public class DateUtil
    {
        static public const MILLISECOND : int = 0;
        static public const SECOND      : int = 1;
        static public const MINUTE      : int = 2;
        static public const HOUR        : int = 3;
        static public const DAY         : int = 4;
        static public const WEEK        : int = 5;
        static public const MONTH       : int = 6;
        static public const YEAR        : int = 7;

        /**
         * return given date starting at given start
         * @param date given date
         * @param start given starting point (year, month, week, etc : default set to millisecond)
         * @return the date reset to some given starting point
         */
        static public function getStartOf ( date : Date, start : int ) : Date
        {
            if ( !date )
                return date;

            var updated : Date;

            switch( start )
            {
                case YEAR:
                {
                    updated = DateUtils.getStartOfYear( date );
                    break;
                }

                case MONTH:
                {
                    updated = new Date( date.fullYear, date.month, 1, 0, 0, 0, 0 );
                    break;
                }

                case WEEK:
                {
                    updated = DateUtils.getStartOfWeek( date );
                    break;
                }

                case DAY:
                {
                    updated = DateUtils.getStartOfDay( date );
                    break;
                }

                case HOUR:
                {
                    updated = new Date( date.fullYear, date.month, date.date, date.hours, 0, 0, 0 );
                    break;
                }

                case MINUTE:
                {
                    updated = new Date( date.fullYear, date.month, date.date, date.hours, date.minutes, 0, 0 );
                    break;
                }

                case SECOND:
                {
                    updated = new Date( date.fullYear, date.month, date.date, date.hours, date.minutes, date.seconds, 0 );
                    break;
                }

                default: // MILLISECONDS
                {
                    updated = new Date( date.fullYear, date.month, date.date, date.hours, date.minutes, date.seconds, date.milliseconds );
                    break;
                }
            }

            return updated;
        }

        /**
         * compare 2 dates
         *
         * @param first the first date to compare
         * @param second the second date to compare
         * @return <code>-1<code> if first date is more recent, <code>1</code> if it is older, <code>0</code> if both are strictly equals (up to the millisecond)
         * @see http://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/Date.html#getTime()
         */
        static public function compare ( first : Date, second : Date, start : int = 0 ) : int
        {
            if ( !first  && !second )
                return 0;

            if ( first && !second )
                return 1;

            if ( !first && second )
                return -1;

            var updated_first : Date = getStartOf( first, start );
            var updated_second : Date = getStartOf( second, start );

            var first_timestamp : Number = updated_first.getTime();
            var second_timestamp : Number = updated_second.getTime();

            if ( first_timestamp < second_timestamp )
                return -1;

            if ( first_timestamp > second_timestamp )
                return 1;

            return 0;
        }

        /**
         * determines if the date is defined as 00-00-0000:00:00:00
         * note : regarding to <code>Date</code> it corresponds to 00-11-1899:00:00:00
         *
         * @param date the date
         * @return <code>true</code> if filled with zero, <code>false</code> otherwise
         */
        static public function isZeroFilled ( date : Date ) : Boolean
        {
            if ( !date )
                return false;

            var day         : Boolean = ( date.day == 0 );
            var month       : Boolean = ( date.month == 11 );
            var year        : Boolean = ( date.fullYear == 1899 );
            var hour        : Boolean = ( date.hours == 0 );
            var minute      : Boolean = ( date.minutes == 0 );
            var second      : Boolean = ( date.seconds == 0 );
            var millisecond : Boolean = ( date.milliseconds == 0 );

            return ( day && month && year && hour && minute && second && millisecond );
        }

        static public function addWeeks ( date : Date, weeks : Number ) : Date
        {
            var days : Number = weeks * 7;
            return addDays( date, days );
        }

        static public function addDays ( date : Date, days : Number ) : Date
        {
            var hours : Number = days * 24;
            return addHours( date, hours );
        }

        static public function addHours ( date : Date, hours : Number ) : Date
        {
            var minutes : Number = hours * 60;
            return addMinutes( date, minutes );
        }

        static public function addMinutes ( date : Date, minutes : Number ) : Date
        {
            var seconds : Number = minutes * 60;
            return addSeconds( date, seconds );
        }

        static public function addSeconds( date : Date, seconds : Number ) : Date
        {
            var milliseconds : Number = seconds * 1000;
            return addMilliseconds( date, milliseconds );
        }

        static public function addMilliseconds( date : Date, milliseconds : Number ) : Date
        {
            var updated : Number = date.getTime() + milliseconds;
            return new Date( updated );
        }

        static public function removeWeeks ( date : Date, weeks : Number ) : Date
        {
            var days : Number = weeks * 7;
            return removeDays( date, days );
        }

        static public function removeDays ( date : Date, days : Number ) : Date
        {
            var hours : Number = days * 24;
            return removeHours( date, hours );
        }

        static public function removeHours ( date : Date, hours : Number ) : Date
        {
            var minutes : Number = hours * 60;
            return removeMinutes( date, minutes );
        }

        static public function removeMinutes ( date : Date, minutes : Number ) : Date
        {
            var seconds : Number = minutes * 60;
            return removeSeconds( date, seconds );
        }

        static public function removeSeconds( date : Date, seconds : Number ) : Date
        {
            var milliseconds : Number = seconds * 1000;
            return removeMilliseconds( date, milliseconds );
        }

        static public function removeMilliseconds( date : Date, milliseconds : Number ) : Date
        {
            var updated : Number = date.getTime() - milliseconds;
            return new Date( updated );
        }
    }
}
