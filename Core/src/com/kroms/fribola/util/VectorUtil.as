/**
 * Created by Romain KELIFA on 30/10/2014.
 */
package com.kroms.fribola.util
{
    public class VectorUtil
    {
        static public function isEmpty ( vector : Object ) : Boolean
        {
            return ( !vector || vector.length == 0 );
        }

        static public function isNotEmpty ( vector : Object ) : Boolean
        {
            return ( vector && vector.length > 0 );
        }
    }
}
