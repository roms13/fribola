/**
 * Created by Romain KELIFA on 02/01/2015.
 */
package com.kroms.fribola.util
{
    public class DigitUtil
    {
        static public function isNotEmpty ( digit : *, empty : int = -1 ) : Boolean
        {
            if ( !isDigit( digit ) )
                return false;

            if ( isNumber( digit ) )
                return !isNaN( digit ) && ( digit > empty );

            return ( digit > empty );
        }

        static public function isEmpty ( digit : *, empty : int = -1 ) : Boolean
        {
            if ( !isDigit( digit ) )
                return true;

            if ( isNumber( digit ) )
                return isNaN( digit ) || ( digit <= empty );

            return ( digit <= empty );
        }

        static public function isDigit ( digit : * ) : Boolean
        {
            return ( isNumber( digit ) || isInt( digit ) || isUint( digit ) );
        }

        static public function isNumber ( digit : * ) : Boolean
        {
            return ( digit is Number );
        }

        static public function isInt ( digit : * ) : Boolean
        {
            return ( digit is int );
        }

        static public function isUint ( digit : * ) : Boolean
        {
            return ( digit is uint );
        }

        /**
         * compares if first digit equals second digit
         * @param first the first digit
         * @param second the second digit
         * @return <code>true</code> if verified, <code>false</code> otherwise
         */
        static public function equals ( first : *, second : * ) : Boolean
        {
            var first_invalid : Boolean = isEmpty( first );
            var second_invalid : Boolean = isEmpty( second );

            if ( first_invalid || second_invalid )
                return false;

            return ( first == second );
        }

        /**
         * compares if a first whole digit contains the second digit
         * example : if <code>210598</code> contains <code>59</code>
         * @param first the first digit
         * @param second the second digit
         * @return <code>true</code> if verified, <code>false</code> otherwise
         */
        static public function contains ( first : *, second : * ) : Boolean
        {
            var first_invalid : Boolean = isEmpty( first );
            var second_invalid : Boolean = isEmpty( second );

            if ( first_invalid || second_invalid )
                return false;

            var f : String = first.toString();
            var s : String = second.toString();

            return ( f.indexOf( s ) != -1 );
        }

        /**
         * compares if a first whole digit starts with second digit
         * example : if <code>210598</code> starts with <code>2105</code>
         * @param first the first digit
         * @param second the second digit
         * @return <code>true</code> if verified, <code>false</code> otherwise
         */
        static public function starts ( first : *, second : * ) : Boolean
        {
            var first_invalid : Boolean = isEmpty( first );
            var second_invalid : Boolean = isEmpty( second );

            if ( first_invalid || second_invalid )
                return false;

            var f : String = first.toString();
            var s : String = second.toString();

            return ( f.indexOf( s ) == 0 );
        }

        /**
         * fills a string with zero
         * @param string the string to fill
         * @param length the required final length
         * @return the string filled with zero to fit given length
         */
        static public function zeroFilled ( string : String, length : int ) : String
        {
            if ( isEmpty( string ) )
                return string;

            var filled : String = string;

            if ( string.length < length )
            {
                var offset : int = length - string.length;
                for ( var i : int = 0; i < offset; i++ )
                {
                    filled = '0' + filled;
                }

                return filled;
            }

            return filled;
        }

        /**
         * compares strict equality between 2 variables of <code>Number</code> type
         * note : if both numbers are <code>Number.NaN</code> then they are equals
         * @param first the first number to compare
         * @param second the second number to compare
         * @return <code>true</code> if numbers are equals, <code>false</code> otherwise
         * @see http://jacksondunstan.com/articles/151
         */
        static public function sameNumber ( first : Number, second : Number ) : Boolean
        {
            var first_NaN : Boolean = isNaN( first );
            var second_NaN : Boolean = isNaN( second );

            if ( first_NaN )
                return second_NaN;
            else if ( second_NaN )
                return false;

            return ( first == second );
        }
    }
}
