/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 13:24
 */
package com.kroms.fribola.util
{
    import avmplus.getQualifiedClassName;

    import flash.utils.Dictionary;
    import flash.utils.describeType;

    /**
     * utility class to store classes definition from the <code>describeType</code> core method to improve performance by avoiding multiple calls to this method
     * @see flash.utils.describeType
     */
    public class DefinitionUtil
    {
        static private var _dictionary : Dictionary;

        /**
         * add the class definition to the dictionary
         * @param clazz the class to be added
         * @return <code>true</code> if definition is was added, <code>false</code> otherwise
         */
        static public function register ( clazz : Class ) : Boolean
        {
            if ( !clazz )
                return false;

            var name : String = getQualifiedClassName( clazz );
            if ( contains( name ) )
                return false;

            var description : XML = describeType( name );
            return _register( name, description );
        }

        /**
         * remove the class definition from the dictionary
         * @param clazz the class to be removed
         * @return <code>true</code> if definition is was removed, <code>false</code> otherwise
         */
        static public function unregister ( clazz : Class ) : Boolean
        {
            if ( !clazz )
                return false;

            var name : String = getQualifiedClassName( clazz );
            return _unregister( name );
        }

        /**
         * add the definition to the dictionary
         * @param name the class name, as returned by <code>getQualifiedClassName</code> core method
         * @param description the class XML description, as returned by <code>describeType</code> core method
         * @return <code>true</code> if definition is was added, <code>false</code> otherwise
         */
        static private function _register ( name : String, description : XML ) : Boolean
        {
            if ( !valid( name, description ) )
                return false;

            if ( !contains( name ) )
            {
                if ( empty ) _dictionary = new Dictionary( true );
                _dictionary[ name ] = description;
            }

            return false;
        }

        /**
         * remove the definition from the dictionary
         * @param name the class name, as returned by <code>getQualifiedClassName</code> core method
         * @return <code>true</code> if definition is was removed, <code>false</code> otherwise
         */
        static private function _unregister ( name : String ) : Boolean
        {
            var description : XML = _dictionary ? _dictionary[ name ] : null;
            if ( description )
            {
                description = null;
                delete ( _dictionary[ name ] );

                return true;
            }

            return false;
        }

        /**
         * determines if the definition of this class is already stored
         * @param name the class name, as returned by <code>getQualifiedClassName</code> core method
         * @return <code>true</code> if definition is already stored, <code>false</code> otherwise
         * @see flash.utils.getQualifiedClassName
         */
        static public function contains ( name : String ) : Boolean
        {
            if ( !empty )
            {
                return ( _dictionary[ name ] != null );
            }

            return false;
        }

        /**
         * determines if this class definition can be stored
         * @param name the class name, as returned by <code>getQualifiedClassName</code> core method
         * @param description the class XML description, as returned by <code>describeType</code> core method
         * @return <code>true</code> if definition is valid, <code>false</code> otherwise
         * @see flash.utils.getQualifiedClassName
         * @see flash.utils.describeType
         */
        // TODO improve with better validity checking
        static private function valid ( name : String, description : XML ) : Boolean
        {
            var valid_name : Boolean = StringUtil.isNotEmpty( name );
            var valid_description : Boolean = ( description != null );

            return ( valid_name && valid_description );
        }

        /**
         * determines whether the dictionary contains any definition
         * @return <code>true</code> if dictionary is empty, <code>false</code> otherwise
         */
        static public function get empty () : Boolean
        {
            return ( !_dictionary && _dictionary.length < 1 );
        }
    }
}
