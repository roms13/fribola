/**
 * Created by Romain KELIFA on 30/10/2014.
 */
package com.kroms.fribola.util
{
    public class ArrayUtil
    {
        static public function isEmpty ( array : Array ) : Boolean
        {
            return ( !array || array.length == 0 );
        }

        static public function isNotEmpty ( array : Array ) : Boolean
        {
            return ( array && array.length > 0 );
        }
    }
}
