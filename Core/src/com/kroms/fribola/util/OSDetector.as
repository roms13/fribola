/**
 * Created by Romain KELIFA on 07/10/2014.
 */
package com.kroms.fribola.util
{

    import flash.system.Capabilities;

    public class OSDetector
    {
        static public const OS : String = Capabilities.version;
        static public const IOS : String = "IOS";
        static public const BLACKBERRY : String = "QNX";
        static public const ANDROID : String = "AND";

        static public function get isIOS () : Boolean
        {
            return ( OS.indexOf(IOS) != -1 );
        }

        static public function get isBlackBerry () : Boolean
        {
            return ( OS.indexOf(BLACKBERRY) != -1 );
        }

        static public function get isAndroid () : Boolean
        {
            return ( OS.indexOf(ANDROID) != -1 );
        }
    }
}
