/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 03/12/2015
 * Time: 22:42
 */
package com.kroms.fribola.util
{

    import com.kroms.fribola.term.AS3DataType;
    import com.kroms.fribola.term.SQL;
    import com.kroms.fribola.term.SQLiteDataType;

    public final class SQLUtil
    {
        static public function getSQLDataType ( as3 : String ) : String
        {
            if ( StringUtil.isEmpty( as3 ) )
                return null;

            switch ( as3 )
            {
                case AS3DataType.STRING:
                {
                    return SQLiteDataType.TEXT;
                }

                case AS3DataType.UINT:
                case AS3DataType.INT:
                {
                    return SQLiteDataType.INTEGER;
                }

                case AS3DataType.NUMBER:
                {
                    return SQLiteDataType.REAL;
                }

                case AS3DataType.BOOLEAN:
                {
                    return SQLiteDataType.BOOLEAN;
                }

                case AS3DataType.DATE:
                {
                    return SQLiteDataType.DATE;
                }
            }

            return null;
        }

        static public function quoteEach ( item : *, index : int, array : Array ) : void
        {
            array[index] = SQL.SINGLE_QUOTE + item + SQL.SINGLE_QUOTE;
        }

        static public function aliasEach ( item : *, index : int, array : Array ) : void
        {
            array[index] = SQL.AROBASE + item;
        }
    }
}
