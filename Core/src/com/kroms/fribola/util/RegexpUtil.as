/**
 * Created by Romain KELIFA on 04/01/2015.
 */
package com.kroms.fribola.util
{
    public class RegexpUtil
    {
        static public function insensitive ( string : String ) : RegExp
        {
            if ( StringUtil.isEmpty( string ) )
                return null;

            var pattern : RegExp = new RegExp( string, 'i' );
            return pattern;
        }

        static public function global ( string : String ) : RegExp
        {
            if ( StringUtil.isEmpty( string ) )
                return null;

            var pattern : RegExp = new RegExp( string, 'g' );
            return pattern;
        }
    }
}
