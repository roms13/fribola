/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 16:40
 */
package com.kroms.fribola.util
{
    import flash.desktop.NativeApplication;

    public class DescriptorUtil
    {
        static private var _APPLICATION_DESCRIPTOR : XML;
        static protected var ns : Namespace;
        /**
         * application descriptor file :
         * this is <code>application descriptor</code> found in the project(s)
         * used by <code>Adobe AIR</code> to compile application
         *
         * @return <code>XML</code> content from <code>application descriptor</code> file
         * @see http://help.adobe.com/en_US/air/build/WS5b3ccc516d4fbf351e63e3d118666ade46-7ff1.html
         */
        static protected function get APPLICATION_DESCRIPTOR () : XML
        {
            if ( !_APPLICATION_DESCRIPTOR )
            {
                _APPLICATION_DESCRIPTOR = NativeApplication.nativeApplication.applicationDescriptor;
                ns = _APPLICATION_DESCRIPTOR.namespace();
            }

            return _APPLICATION_DESCRIPTOR;
        }

        static private var _APPLICATION_ID : String;
        /**
         * application unique identifier :
         * extracted from <code>application descriptor</code>
         * used by <code>Adobe AIR</code> for application identification : MUST BE UNIQUE
         * @return look at the different values in <code>application descriptor</code> in the <code>id</code> node
         */
        static public function get APPLICATION_ID () : String
        {
            if ( !_APPLICATION_ID )
            {
                _APPLICATION_ID = APPLICATION_DESCRIPTOR.ns::id[0];
            }

            return _APPLICATION_ID;
        }

        static private var _APPLICATION_VERSION_NUMBER : String;
        /**
         * application version number :
         * extracted from <code>application descriptor</code>
         * used by <code>Adobe AIR</code> for application update
         * @return look at the different values in <code>application descriptor</code> in the <code>versionNumber</code> node
         */
        static public function get APPLICATION_VERSION_NUMBER () : String
        {
            if ( !_APPLICATION_VERSION_NUMBER )
            {
                _APPLICATION_VERSION_NUMBER = APPLICATION_DESCRIPTOR.ns::versionNumber[0];
            }

            return _APPLICATION_VERSION_NUMBER;
        }

        static private var _APPLICATION_FILENAME : String;
        /**
         * application install filename :
         * extracted from <code>application descriptor</code>
         * used by <code>Adobe AIR</code> on application install file generation
         * @return look at the different values in <code>application descriptor</code> in the <code>filename</code> node
         */
        static public function get APPLICATION_FILENAME () : String
        {
            if ( !_APPLICATION_FILENAME )
            {
                _APPLICATION_FILENAME = APPLICATION_DESCRIPTOR.ns::filename[0];
            }

            return _APPLICATION_FILENAME;
        }
    }
}
