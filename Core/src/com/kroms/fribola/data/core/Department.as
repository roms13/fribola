/**
 * Created by Romain KELIFA on 30/12/2014.
 */
package com.kroms.fribola.data.core
{
    import com.kroms.fribola.vo.ValueObject;

    [RemoteClass(alias="com.kroms.fribola.vo.Department")]
    public class Department extends ValueObject
    {
        private var _code           : String;
        private var _denomination   : String;
        private var _slug           : String;

        public function get code () : String { return _code; }
        public function set code ( value : String ) : void
        {
            if ( _code == value )
                return;

            _code = value;
        }

        public function get denomination () : String { return _denomination; }
        public function set denomination ( value : String ) : void
        {
            if ( _denomination == value )
                return;

            _denomination = value;
        }

        public function get slug () : String { return _slug; }
        public function set slug ( value : String ) : void
        {
            if( _slug == value )
                return;

            _slug = value;
        }
    }
}
