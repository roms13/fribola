/**
 * Created by Romain KELIFA on 31/12/2014.
 */
package com.kroms.fribola.data.core
{
    import com.kroms.fribola.vo.UniqueObject;

    [RemoteClass(alias="com.kroms.fribola.vo.City")]
    public class City extends UniqueObject
    {
        private var _denomination       : String;
        private var _zipcode            : String;
        private var _department_uid    : String;

        public function get denomination () : String { return _denomination; }
        public function set denomination ( value : String ) : void
        {
            if ( _denomination == value )
                return;

            _denomination = value;
        }

        public function get zipcode () : String { return _zipcode; }
        public function set zipcode ( value : String ) : void
        {
            if ( _zipcode == value )
                return;

            _zipcode = value;
        }

        public function get department_uid () : String { return _department_uid; }
        public function set department_uid ( value : String ) : void
        {
            if ( _department_uid == value )
                return;

            _department_uid = value;
        }
    }
}
