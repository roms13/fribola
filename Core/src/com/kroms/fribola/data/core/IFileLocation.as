/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 15:06
 */
package com.kroms.fribola.data.core
{
    public interface IFileLocation
    {
        function get filename () : String;
        function get extension () : String;
    }
}
