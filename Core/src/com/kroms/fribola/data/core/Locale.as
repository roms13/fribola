/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 16:28
 */
package com.kroms.fribola.data.core
{
    public class Locale
    {
        public var codeLanguage : String;
        public var codeCountry : String;
        public var language : String;
        public var country : String;

        public function Locale ( codeLanguage : String, codeCountry : String, language : String, country : String )
        {
            super();

            this.codeLanguage = codeLanguage;
            this.codeCountry = codeCountry;
            this.language = language;
            this.country = country;
        }

        public function get localeID () : String { return [ codeLanguage, codeCountry ].join('-'); }
        public function get locale () : String { return [ codeLanguage, codeCountry ].join('_'); }
    }
}
