/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 15:08
 */
package com.kroms.fribola.data.core
{
    public interface IFile extends IFileLocation, IBinary
    {
    }
}
