/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 14:56
 */
package com.kroms.fribola.data.core
{
    import org.as3commons.zip.Zip;

    public interface IZip
    {
        function get zip () : Zip;
    }
}
