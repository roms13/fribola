/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 03/12/2015
 * Time: 22:29
 */
package com.kroms.fribola.term
{

    public final class SQL
    {
        static public const CREATE              : String = "CREATE";
        static public const DROP                : String = "DROP";
        static public const ALTER               : String = "ALTER";
        static public const RENAME_TO           : String = "RENAME TO";
        static public const SELECT              : String = "SELECT";
        static public const UPDATE              : String = "UPDATE";
        static public const DELETE              : String = "DELETE";
        static public const INSERT              : String = "INSERT";

        static public const PRIMARY_KEY         : String = "PRIMARY KEY";
        static public const UNIQUE              : String = "UNIQUE";
        static public const AUTO_INCREMENT      : String = "AUTO INCREMENT";

        static public const SPACE               : String = " ";
        static public const ASTERISK            : String = "*";
        static public const ALL                 : String = "ALL";
        static public const TABLE               : String = "TABLE";

        static public const FROM                : String = "FROM";
        static public const SET                 : String = "SET";
        static public const INTO                : String = "INTO";
        static public const VALUES              : String = "VALUES";
        static public const WHERE               : String = "WHERE";
        static public const IGNORE              : String = "IGNORE";

        static public const IN                  : String = "IN";
        static public const AND                 : String = "AND";
        static public const OR                  : String = "OR";

        static public const AS                  : String = "AS";
        static public const ADD                 : String = "ADD";
        static public const UNION               : String = "UNION";

        static public const IF_NOT_EXISTS       : String = "IF NOT EXISTS";
        static public const IF_EXISTS           : String = "IF EXISTS";

        static public const PARENTHESIS_OPEN    : String = "(";
        static public const PARENTHESIS_CLOSE   : String = ")";

        static public const COMMA               : String = ",";
        static public const DOT                 : String = ".";
        static public const EQUALS              : String = "=";
        static public const DIFFERENT           : String = "<>"; // can also be written as "!="
        static public const AROBASE             : String = "@";
        static public const ORDER_BY            : String = "ORDER BY";
        static public const ASC                 : String = "ASC";
        static public const SINGLE_QUOTE        : String = "'";
        static public const DOUBLE_QUOTE        : String = '"';
        static public const SPECIAL_QUOTE       : String = "`";
        static public const UNDERSCORE          : String = "_";
        static public const SEMICOLON           : String = ";";

        static public const NULL                : String = "NULL";
        static public const TEMPORARY           : String = "TEMPORARY";
    }
}
