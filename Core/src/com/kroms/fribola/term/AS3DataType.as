/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 03/12/2015
 * Time: 23:35
 */
package com.kroms.fribola.term
{

    public final class AS3DataType
    {
        static public const BOOLEAN     : String = 'Boolean';
        static public const INT         : String = 'int';
        static public const UINT        : String = 'uint';
        static public const NUMBER      : String = 'Number';
        static public const XML         : String = 'XML';
        static public const DATE        : String = 'Date';
        static public const STRING      : String = 'String';
        static public const BYTEARRAY   : String = 'ByteArray';
        static public const ARRAY       : String = 'Array';
    }
}
