/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 03/12/2015
 * Time: 22:57
 */
package com.kroms.fribola.term
{

    /**
     * Here are the main points:
     *
     * The following column affinity types are not supported by default in SQLite, but are supported in Adobe AIR:
     *
     * STRING: corresponding to the String class (equivalent to the TEXT column affinity).
     * NUMBER: corresponding to the Number class (equivalent to the REAL column affinity).
     * BOOLEAN: corresponding to the Boolean class.
     * DATE: corresponding to the Date class.
     * XML: corresponding to the ActionScript (E4X) XML class.
     * XMLLIST: corresponding to the ActionScript (E4X) XMLList class.
     * OBJECT: corresponding to the Object class or any subclass that can be serialized and deserialized using AMF3. (This includes most classes including custom classes, but excludes some classes including display objects and objects that include display objects as properties.)
     * The following literal values are not supported by default in SQLite, but are supported in Adobe AIR:
     *
     * true: used to represent the literal boolean value true, for working with BOOLEAN columns.
     * false: used to represent the literal boolean value false, for working with BOOLEAN columns.
     * Each column in the database is assigned one of the following type affinities:
     *
     * TEXT (or STRING)
     * NUMERIC
     * INTEGER
     * REAL (or NUMBER)
     * BOOLEAN
     * DATE
     * XML
     * XMLLIST
     * OBJECT
     * NONE
     *
     * @see http://samhassan.co.uk/2009/11/30/sqlite-data-types/
     * @see http://livedocs.adobe.com/flex/3/langref/localDatabaseSQLSupport.html#dataTypes
     */
    public final class SQLiteDataType
    {
        // AIR specifics
        static public const STRING : String = 'STRING';
        static public const NUMBER : String = 'NUMBER';
        static public const BOOLEAN : String = 'BOOLEAN';
        static public const DATE : String = 'DATE';
        static public const XML : String = 'XML';
        static public const XMLLIST : String = 'XMLLIST';
        static public const OBJECT : String = 'OBJECT';

        // SQLite specifics
        static public const TEXT : String = 'TEXT';
        static public const NUMERIC : String = 'NUMERIC';
        static public const INTEGER : String = 'INTEGER';
        static public const REAL : String = 'REAL';
    }
}
