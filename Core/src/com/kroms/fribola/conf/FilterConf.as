/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 14/12/2015
 * Time: 12:01
 */
package com.kroms.fribola.conf
{
    public class FilterConf
    {
        static public const FILTER_KEYWORD_MINIMUM_LENGTH   : uint = 3;
        static public const FILTER_KEYWORD_MINIMUM_DELAY    : uint = 200;
    }
}
