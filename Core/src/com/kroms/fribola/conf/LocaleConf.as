/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 16:30
 */
package com.kroms.fribola.conf
{
    import com.kroms.fribola.data.core.Locale;

    public class LocaleConf
    {
        static public const FRENCH_CODE_LANGUAGE    : String = "fr";
        static public const FRENCH_CODE_COUNTRY     : String = "FR";
        static public const FRENCH_NAME_LANGUAGE    : String = "français";
        static public const FRENCH_NAME_COUNTRY     : String = "France";
        static public const FRENCH : Locale = new Locale( FRENCH_CODE_LANGUAGE, FRENCH_CODE_COUNTRY, FRENCH_NAME_LANGUAGE, FRENCH_NAME_COUNTRY );

        static public const AVAILABLE_LOCALES : Vector.<Locale> = new <Locale>[
            FRENCH
        ];
    }
}
