/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 14/12/2015
 * Time: 11:45
 */
package com.kroms.fribola.conf
{
    public class LocationConf
    {
        static public const CITY_ZIPCODE_DELIMITER          : String = '-';
        static public const FRENCH_ZIPCODE_LENGTH           : uint = 5;
    }
}
