/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 20:39
 */
package com.kroms.fribola.component
{
    import com.kroms.fribola.event.ChoiceEvent;

    import flash.events.MouseEvent;

    import spark.components.Button;
    import spark.components.HGroup;

    /**
     * callout implementation with Valid and Cancel buttons
     * TODO : improve event listeners handling keeping in mind that callout can be opened / closed multiple times
     */
    [Event(name="confirm",type="com.kroms.fribola.event.ChoiceEvent")]
    [Event(name="cancel",type="com.kroms.fribola.event.ChoiceEvent")]
    public class ConfirmCallout extends MessageCallout
    {
        public var confirmButton : Button;
        public var cancelButton : Button;

        public function ConfirmCallout ()
        {
            super ();
        }

        override protected function createChildren () : void
        {
            super.createChildren ();

            confirmButton = new Button();
            confirmButton.addEventListener( MouseEvent.CLICK, onValidClick );

            cancelButton = new Button();
            cancelButton.addEventListener( MouseEvent.CLICK, onCancelClick );

            var bar : HGroup = new HGroup();
            bar.percentWidth = 100;
            bar.addElement( confirmButton );
            bar.addElement( cancelButton );

            addElement( bar );
        }

        protected function updateUI () : void
        {}

        protected function onValidClick ( e : MouseEvent ) : void
        {
            dispatchEvent( new ChoiceEvent( ChoiceEvent.CONFIRM ) );
            close();
        }

        protected function onCancelClick ( e : MouseEvent ) : void
        {
            dispatchEvent( new ChoiceEvent( ChoiceEvent.CANCEL ) );
            close();
        }
    }
}
