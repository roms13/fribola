/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 22:05
 */
package com.kroms.fribola.component
{
    public class EditionCallout extends ConfirmCallout
    {
        public function EditionCallout ()
        {
            super ();
        }

        override protected function createChildren () : void
        {
            super.createChildren ();
            updateUI();
        }

        override protected function updateUI () : void
        {
            super.updateUI();

            if ( confirmButton )
            {
                confirmButton.enabled = edited;
            }
        }

        protected function get edited () : Boolean { return false; }
    }
}
