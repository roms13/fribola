/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 21:17
 */
package com.kroms.fribola.component
{
    import com.kroms.fribola.util.StringUtil;

    import flash.display.DisplayObjectContainer;

    import spark.components.TextInput;
    import spark.events.TextOperationEvent;

    public class InputCallout extends EditionCallout
    {
        protected var input : TextInput;

        private var _value : String;
        private var _initial : String;
        protected var valueChange : Boolean;

        public function InputCallout ()
        {
            super ();
        }

        override protected function createChildren () : void
        {
            super.createChildren ();
            input = new TextInput();
            input.percentWidth = 100;
            input.addEventListener(TextOperationEvent.CHANGE, onTextChange);

            updateValue();

            addElementAt( input, 1 );
        }

        override protected function commitProperties () : void
        {
            super.commitProperties ();

            if ( valueChange )
            {
                updateValue();
                valueChange = false;
            }
        }

        override public function open(owner:DisplayObjectContainer, modal:Boolean=false):void
        {
            super.open(owner,modal);
            selectAll();
        }

        protected function onTextChange ( e : TextOperationEvent ) : void
        {
            _value = input.text;
            updateUI();
        }

        protected function selectAll () : void
        {
            if ( input )
            {
                var length : int = _value ? _value.length : 0;
                input.selectRange(0,length);
                input.setFocus();
            }
        }

        protected function updateValue () : void
        {
            if ( input )
            {
                input.text = _value;
            }
        }

        override protected function updateUI () : void
        {
            super.updateUI();

            if ( confirmButton )
            {
                confirmButton.enabled = edited;
            }
        }

        override protected function get edited () : Boolean
        {
            var filled : Boolean = StringUtil.isNotEmpty( _value );
            var edited : Boolean = ( _value != _initial );
            return ( filled && edited );
        }

        public function get value () : String { return _value; }
        public function set value ( value : String ) : void
        {
            if ( _value == value )
                return;

            _value = value;
            _initial = value;
            valueChange = true;
            invalidateProperties();
        }
    }
}
