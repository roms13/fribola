/**
 * Created by Romain KELIFA on 03/01/2015.
 */
package com.kroms.fribola.component
{
    import com.kroms.fribola.renderer.CityRenderer;
    import com.kroms.fribola.util.CityUtil;

    import mx.collections.IList;

    import org.apache.flex.collections.VectorCollection;

    /**
     * pop-up with french cities list and searchable by keywords implementation
     */
    public class CityCallout extends SearchableListingCallout
    {
        public function CityCallout ()
        {
            super ();

            renderer = CityRenderer;
            message = resourceManager.getString('fribola','core.city.selection');
            emptyMessage = resourceManager.getString('fribola','core.city.none');
        }

        override protected function filter_by_keywords ( item : Object ) : Boolean
        {
            return CityUtil.filter_by_keywords( item, this.keywords );
        }

        private var faster_cities : VectorCollection;
        override public function get dataProvider () : IList { return faster_cities; }
        override public function set dataProvider(value:IList):void
        {
            // beware here that we are given an ArrayCollection, but we also know that model.cities is defined too
            if ( ( value && !faster_cities ) || ( faster_cities && !value ) || ( faster_cities.length != value.length ) )
            {
                if ( value )
                {
                    faster_cities = new VectorCollection();
                    faster_cities.source = CityUtil.getCitiesFasterCopy(); // get it directly from model
                }
                else
                    faster_cities = null;
            }

            super.dataProvider = value;
        }
    }
}
