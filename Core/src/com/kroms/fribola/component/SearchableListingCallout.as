/**
 * Created by Romain KELIFA on 15/12/2014.
 */
package com.kroms.fribola.component
{
    import com.kroms.fribola.conf.FilterConf;
    import com.kroms.fribola.util.StringUtil;

    import flash.display.DisplayObjectContainer;
    import flash.events.Event;
    import flash.events.TimerEvent;
    import flash.utils.Timer;

    import mx.collections.ICollectionView;

    import spark.components.Label;
    import spark.components.TextInput;
    import spark.events.TextOperationEvent;

    /**
     * pop-up with keywords searchable list implementation
     */
    public class SearchableListingCallout extends ListingCallout
    {
        protected var searchInput : TextInput;
        protected var searchLabel : Label;

        private var _keywords : String;
        protected var filter_timer : Timer;

        public function SearchableListingCallout ()
        {
            super ();
        }

        private function onTextChange ( e : TextOperationEvent ) : void
        {
            keywords = searchInput.text;
            selectedIndex = -1;
            updateUI();
        }

        protected function updateInput () : void
        {
            if ( searchInput )
                searchInput.text = _keywords;

            updateFiltering();
        }

        protected function updateFiltering () : void
        {
            if ( filter_timer.running )
                filter_timer.reset();

            filter_timer.start();
        }

        protected function onDelay ( e : TimerEvent ) : void
        {
            if ( dataProvider )
            {
                var collection : ICollectionView = ( dataProvider as ICollectionView );
                collection.filterFunction = StringUtil.isNotEmpty( _keywords ) ? filter_by_keywords : null;
                collection.refresh();
            }

            filter_timer.reset();
        }

        protected function filter_by_keywords ( item : Object ) : Boolean
        {
            var not_null : Boolean = ( item != null );
            var no_keywords : Boolean = this.keywords && StringUtil.isNotEmpty( this.keywords );

            return ( not_null || no_keywords );
        }

        override public function open ( owner : DisplayObjectContainer, modal : Boolean = false ) : void
        {
            filter_timer = new Timer( FilterConf.FILTER_KEYWORD_MINIMUM_DELAY, 1 );
            filter_timer.addEventListener( TimerEvent.TIMER_COMPLETE, onDelay );

            super.open ( owner, modal );

            searchInput.setFocus();

            keywords = null;
            selectedIndex = -1;
            updateUI();
        }

        override public function close ( commit : Boolean = false, data : * = null ) : void
        {
            super.close ( commit, data );

            filter_timer.removeEventListener( TimerEvent.TIMER_COMPLETE, onDelay );
            filter_timer = null;
        }

        override protected function createChildren () : void
        {
            super.createChildren ();

            searchLabel = new Label();
            searchLabel.text = resourceManager.getString('fribola','core.filter.by.keywords');

            searchInput = new TextInput();
            searchInput.percentWidth = 100;
            searchInput.addEventListener(TextOperationEvent.CHANGE, onTextChange);

            addElementAt(searchLabel,1);
            addElementAt(searchInput,2);
        }

        override protected function childrenCreated () : void
        {
            super.childrenCreated();

            updateInput();
        }

        public function get keywords () : String { return _keywords; }
        public function set keywords ( value : String ) : void
        {
            if ( _keywords == value )
                return;

            _keywords = value;
            updateInput();
        }
    }
}
