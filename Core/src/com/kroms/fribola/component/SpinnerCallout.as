/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 21:34
 */
package com.kroms.fribola.component
{
    import flash.display.DisplayObjectContainer;
    import flash.events.Event;

    import org.as3commons.lang.DateUtils;

    import spark.components.DateSpinner;
    import spark.components.calendarClasses.DateSelectorDisplayMode;

    public class SpinnerCallout extends EditionCallout
    {
        public var spinner : DateSpinner;

        private var _value : Date;
        private var _initial : Date;

        public function SpinnerCallout ()
        {
            super ();
        }

        override protected function createChildren():void
        {
            super.createChildren();

            spinner = new DateSpinner();
            spinner.percentWidth = 100;
            spinner.displayMode = DateSelectorDisplayMode.DATE;
            spinner.addEventListener(Event.CHANGE, onDateChange);

            updateValue();
            updateMaxValue();

            addElementAt( spinner, 1 );
        }

        override public function open(owner:DisplayObjectContainer, modal:Boolean = false):void
        {
            super.open(owner, modal);

            updateMaxValue();
        }

        protected function onDateChange ( e : Event ) : void
        {
            _value = DateUtils.getStartOfDay( spinner.selectedDate );

            updateValue();
            updateUI();
        }

        protected function updateValue () : void
        {
            if ( spinner )
            {
                spinner.selectedDate = _value;
            }
        }

        protected function updateMaxValue () : void
        {
            if ( spinner )
            {
                spinner.maxDate = new Date();
            }
        }

        public function get value():Date { return _value; }
        public function set value( value : Date ) : void
        {
            if ( _value == value )
                return;

            _value = value;
            _initial = value;

            updateValue();
            updateUI();
        }

        override protected function get edited () : Boolean
        {
            var filled : Boolean = ( _value != null );
            var anterior : Boolean = new Date() > _value;
            var edited : Boolean = ( _value != _initial );
            return ( filled && anterior && edited );
        }
    }
}
