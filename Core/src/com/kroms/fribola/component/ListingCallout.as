/**
 * Created by Romain KELIFA on 02/12/2014.
 */
package com.kroms.fribola.component
{
    import flash.events.MouseEvent;

    import mx.collections.IList;
    import mx.core.ClassFactory;

    import spark.components.Group;
    import spark.components.Label;
    import spark.components.List;
    import spark.components.supportClasses.ItemRenderer;
    import spark.events.IndexChangeEvent;

    /**
     * pop-up with list implementation
     */
    public class ListingCallout extends ConfirmCallout
    {
        protected var list : List;
        protected var emptyLabel : Label;
        protected var listGroup : Group;

        protected var _selectedIndex : int = -1;
        private var _selectedItem : Object;
        private var _dataProvider : IList;

        private var _emptyMessage : String;

        private var _renderer : Class;

        public function ListingCallout()
        {
            super();
        }

        override protected function createChildren () : void
        {
            super.createChildren ();

            listGroup = new Group();
            listGroup.horizontalCenter = 0;
            listGroup.verticalCenter = 0;

            list = new List();
            list.percentWidth = 100;
            list.minHeight = 0;
            list.maxHeight = 300;
            list.doubleClickEnabled = true;
            list.useVirtualLayout = true;
            list.addEventListener(IndexChangeEvent.CHANGE, onSelectionChange);
            list.addEventListener(MouseEvent.DOUBLE_CLICK, onDoubleClick);
            list.requireSelection = hasSelection;

            emptyLabel = new Label();
            emptyLabel.text = message;

            listGroup.addElement( emptyLabel );
            listGroup.addElement( list );

            addElementAt(listGroup,1);
        }

        override protected function childrenCreated () : void
        {
            super.childrenCreated();

            updateProvider();
            updateEmptyMessage();
            updateRenderer();
        }

        protected function onSelectionChange ( e : IndexChangeEvent ) : void
        {
            _selectedIndex = list.selectedIndex;

            updateSelectedIndex();
            updateUI();
        }

        protected function onDoubleClick ( e : MouseEvent ) : void
        {
            onValidClick(e);
        }

        protected function updateProvider () : void
        {
            if ( list )
            {
                list.dataProvider = dataProvider;
                list.visible = ( dataProvider && dataProvider.length > 0 );
                listGroup.invalidateSize();
                updateSelectedIndex();
            }
        }

        protected function updateSelectedIndex () : void
        {
            if ( list )
            {
                var provider : IList = list.dataProvider;
                var count : int = provider ? provider.length : 0;
                var nonNullIndex : Boolean = _selectedIndex >= 0;
                var validIndex : Boolean = _selectedIndex < count;

                if ( nonNullIndex && validIndex )
                {
                    list.selectedIndex = _selectedIndex;
                    _selectedItem = provider.getItemAt( _selectedIndex );
                }
                else
                {
                    list.selectedIndex = -1;
                    _selectedIndex = -1;
                    _selectedItem = null;
                }
            }
        }

        protected function updateSelectedItem () : void
        {
            if ( list )
            {
                var count : int = dataProvider ? dataProvider.length : 0;
                var has : Boolean = count > 0;
                var found : Boolean;
                var item : Object;

                if ( has )
                {
                    for ( var i : int = 0; i < count; i++ )
                    {
                        item = dataProvider.getItemAt(i);

                        if ( _selectedItem == item )
                        {
                            list.selectedItem = _selectedItem;
                            _selectedIndex = i;
                            found = true;
                        }
                    }

                }

                if ( !found )
                {
                    list.selectedItem = null;
                    list.selectedIndex = -1;
                    _selectedIndex = -1;
                    _selectedItem = null;
                }

                updateUI();
            }
        }

        protected function updateEmptyMessage () : void
        {
            if ( emptyLabel )
            {
                emptyLabel.text = _emptyMessage;
            }
        }

        protected function updateRenderer () : void
        {
            if ( list && _renderer )
            {
                list.itemRenderer = new ClassFactory( _renderer );
            }
        }

        override protected function updateUI () : void
        {
            super.updateUI();

            if ( list )
            {
                list.requireSelection = hasSelection && hasDisplayedItem;
                list.validateNow();
            }

            if ( confirmButton )
                confirmButton.enabled = hasSelection;
        }

        protected function get hasDisplayedItem () : Boolean
        {
            return ( dataProvider && ( dataProvider.length > 0 ) );
        }

        protected function get hasSelection () : Boolean
        {
            var validIndex : Boolean = _selectedIndex != -1;
            var validItem : Boolean = _selectedItem != null;

            return ( validIndex || validItem );
        }

        public function get selectedIndex () : int { return _selectedIndex; }
        public function set selectedIndex ( value : int ) : void
        {
            _selectedIndex = value;
            updateSelectedIndex();
        }

        public function get selectedItem():Object { return _selectedItem; }
        public function set selectedItem(value:Object):void
        {
            _selectedItem = value;
            updateSelectedItem();
        }

        public function get dataProvider():IList { return _dataProvider; }
        public function set dataProvider(value:IList):void
        {
            if ( _dataProvider == value )
                return;

            _dataProvider = value;
            updateProvider();
        }

        public function get emptyMessage():String { return _emptyMessage; }
        public function set emptyMessage(value:String):void {
            if ( _emptyMessage == value )
                return;

            _emptyMessage = value;
            updateEmptyMessage();
        }

        public function get renderer () : Class
        {
            if ( !_renderer )
                return ItemRenderer;

            return _renderer;
        }

        public function set renderer ( value : Class ) : void
        {
            if ( _renderer == value )
                return;

            _renderer = value;
            updateRenderer();
        }
    }
}
