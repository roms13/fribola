/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 20:15
 */
package com.kroms.fribola.component
{
    import spark.components.Callout;
    import spark.components.Label;

    public class MessageCallout extends Callout
    {
        protected var messageDisplay : Label;

        private var _message : String;
        protected var messageChange : Boolean;

        public function MessageCallout ()
        {
            super ();
        }

        override protected function createChildren () : void
        {
            super.createChildren ();

            messageDisplay = new Label();
            messageDisplay.text = _message;
            messageDisplay.setStyle('textAlign', 'center');

            addElement( messageDisplay );
        }

        override protected function commitProperties () : void
        {
            super.commitProperties ();

            if ( messageChange )
            {
                updateMessage();
                invalidateDisplayList();
                messageChange = false;
            }
        }

        protected function updateMessage () : void
        {
            if ( messageDisplay )
            {
                messageDisplay.text = _message;
            }
        }

        public function get message () : String { return _message; }
        public function set message ( value : String ) : void
        {
            if ( _message == value )
                return;

            _message = value;
            messageChange = true;
            invalidateProperties();
        }
    }
}
