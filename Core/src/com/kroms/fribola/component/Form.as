/**
 * Created by Romain KELIFA on 23/12/2014.
 */
package com.kroms.fribola.component
{
    import com.kroms.fribola.vo.ValueObject;
    import com.kroms.fribola.event.FormEvent;

    import flash.events.Event;
    import flash.events.MouseEvent;

    import mx.events.FlexEvent;
    import mx.utils.ObjectUtil;

    import spark.components.Button;
    import spark.components.Form;

    /**
     * basic form implementation
     * included with Submit, Cancel and Reset buttons and their logic
     */
    [Event(name="submit",type="com.kroms.fribola.event.FormEvent")]
    [Event(name="cancel",type="com.kroms.fribola.event.FormEvent")]
    [Event(name="reset",type="com.kroms.fribola.event.FormEvent")]
    public class Form extends spark.components.Form
    {
        [SkinPart(required="true")]
        public var submitButton : Button;

        [SkinPart(required="true")]
        public var cancelButton : Button;

        [SkinPart(required="false")]
        public var resetButton : Button;

        private var _current : ValueObject;

        protected var currentChange : Boolean;

        public function Form ()
        {
            super ();

            addEventListener( FlexEvent.CREATION_COMPLETE, onCreationComplete );
            addEventListener( FlexEvent.REMOVE, onRemove );
        }

        /**
         * @inheritDoc
         */
        override protected function partAdded ( partName : String, instance : Object ) : void
        {
            super.partAdded ( partName, instance );

            if ( instance == submitButton )
            {
                submitButton.label = resourceManager.getString('fribola','core.submit');
                submitButton.addEventListener( MouseEvent.CLICK, onSubmit );
                submitButton.enabled = submitAllowed;
            }

            if ( instance == cancelButton )
            {
                cancelButton.label = resourceManager.getString('fribola','core.cancel');
                cancelButton.addEventListener( MouseEvent.CLICK, onCancel );
            }

            if ( instance == resetButton )
            {
                resetButton.label = resourceManager.getString('fribola','core.reset');
                resetButton.addEventListener( MouseEvent.CLICK, onReset );
                resetButton.enabled = resetAllowed;
            }
        }

        /**
         * @inheritDoc
         */
        override protected function partRemoved ( partName : String, instance : Object ) : void
        {
            super.partRemoved ( partName, instance );

            if ( instance == submitButton )
            {
                submitButton.removeEventListener( MouseEvent.CLICK, onSubmit );
            }

            if ( instance == cancelButton )
            {
                cancelButton.removeEventListener( MouseEvent.CLICK, onCancel );
            }

            if ( instance == resetButton )
            {
                resetButton.removeEventListener( MouseEvent.CLICK, onReset );
            }
        }

        override protected function commitProperties () : void
        {
            super.commitProperties ();

            if ( currentChange )
            {
                updateCurrent();
                currentChange = false;
            }
        }

        protected function onCreationComplete ( e : FlexEvent ) : void
        {
            removeEventListener( FlexEvent.CREATION_COMPLETE, onCreationComplete );
        }

        protected function onRemove ( e : FlexEvent ) : void
        {
            if ( hasEventListener( FlexEvent.CREATION_COMPLETE ) )
                removeEventListener( FlexEvent.CREATION_COMPLETE, onCreationComplete );

            removeEventListener( FlexEvent.REMOVE, onRemove );
        }

        /**
         * on current data change
         */
        protected function updateCurrent () : void
        {}

        /**
         * on field change
         * @param e the event dispatched
         */
        protected function onChange ( e : Event ) : void
        {
            validate();
        }

        /**
         * on cancel click
         * @param e the event dispatched
         */
        protected function onCancel ( e : MouseEvent ) : void
        {
            cancel();
        }

        /**
         * on reset click
         * @param e the event dispatched
         */
        protected function onReset ( e : MouseEvent ) : void
        {
            reset();
        }

        /**
         * submit data changes
         */
        protected function submit () : void
        {
            dispatchEvent( new FormEvent( FormEvent.SUBMIT ) );
        }

        /**
         * cancel any change
         */
        protected function cancel () : void
        {
            dispatchEvent( new FormEvent( FormEvent.CANCEL ) );
        }

        /**
         * reset data changes
         */
        protected function reset () : void
        {
            clear();
            validate();
            dispatchEvent( new FormEvent( FormEvent.RESET ) );
        }

        /**
         * validate form to allow for submit or reset
         */
        protected function validate () : void
        {
            if ( submitButton )
                submitButton.enabled = submitAllowed;

            if ( resetButton )
                resetButton.enabled = resetAllowed;
        }

        /**
         * if data is empty
         */
        protected function get empty () : Boolean
        {
            return ( current && current.empty );
        }

        /**
         * clear data
         */
        protected function clear () : void
        {}

        /**
         * if data is valid
         */
        protected function get valid () : Boolean
        {
            return current && current.valid;
        }

        /**
         * on submit click
         * @param e the event dispatched
         */
        protected function onSubmit ( e : MouseEvent ) : void
        {
            if ( submitAllowed )
            {
                submit();
            }
        }

        /**
         * if submit is allowed
         * @return <code>true</code> if allowed, <code>false</code> otherwise
         */
        protected function get submitAllowed () : Boolean
        {
            return valid;
        }

        /**
         * if reset is allowed
         * @return <code>true</code> if allowed, <code>false</code> otherwise
         */
        protected function get resetAllowed () : Boolean
        {
            return !empty;
        }

        /**
         * the currently edited data
         * @see com.kroms.fribola.vo.ValueObject
         */
        public function get current () : ValueObject { return _current; }
        public function set current ( value : ValueObject ) : void
        {
            if ( current == value )
                return;

            _current = value;

            if ( _current )
                _current.setSource( ObjectUtil.copy( _current ) as ValueObject );

            currentChange = true;
            invalidateProperties();
        }
    }
}
