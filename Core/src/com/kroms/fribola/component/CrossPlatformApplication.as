/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 16:09
 */
package com.kroms.fribola.component
{
    import com.kroms.fribola.conf.LocaleConf;
    import com.kroms.fribola.impl.IApplicationImplementable;
    import com.kroms.fribola.impl.IJPEGEncoderImplementable;
    import com.kroms.fribola.impl.INetworkInfoImplementable;

    import flash.net.registerClassAlias;

    import spark.components.ViewNavigatorApplication;

    [ResourceBundle("fribola")]
    public class CrossPlatformApplication extends ViewNavigatorApplication
    {
        static public var Application : IApplicationImplementable;
        static public var JPEGEncoder : IJPEGEncoderImplementable;
        static public var NetworkInfo : INetworkInfoImplementable;

        public function CrossPlatformApplication ()
        {
            setLocale();
            initializeClassAliases();
            initializeLibraries();
        }

        protected function setLocale () : void
        {
            this.setStyle( "locale", LocaleConf.FRENCH.localeID );
        }

        protected function initializeClassAliases () : void
        {
            registerClassAlias( "String", String );
        }

        protected function initializeLibraries () : void
        {}
    }
}
