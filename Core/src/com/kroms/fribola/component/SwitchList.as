/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 20/12/2015
 * Time: 16:39
 */
package com.kroms.fribola.component
{
    import flash.events.MouseEvent;

    import mx.core.IVisualElement;

    import spark.components.IItemRenderer;

    import spark.components.List;

    public class SwitchList extends List
    {
        public function SwitchList ()
        {
            super ();
        }

        override protected function item_mouseDownHandler ( e : MouseEvent ) : void
        {
            var newIndex:int;
            if (e.currentTarget is IItemRenderer)
                newIndex = IItemRenderer(e.currentTarget).itemIndex;
            else
                newIndex = dataGroup.getElementIndex(e.currentTarget as IVisualElement);

            var same : Boolean = !allowMultipleSelection && ( selectedIndex == newIndex );

            if ( same )
            {
                e.stopImmediatePropagation();
                setSelectedIndex(-1,true,true);
            }
            else
            {
                super.item_mouseDownHandler(e);
            }
        }
    }
}
