/**
 * Created by Romain KELIFA on 29/12/2014.
 */
package com.kroms.fribola.component
{
    import com.kroms.fribola.vo.ValueObject;
    import com.kroms.fribola.event.FormEvent;

    /**
     * advanced form implementation (meant to be used with <code>com.kroms.fribola.vo.ValueObject</code>)
     * allows for quick implementation of data validation, and data revert
     * @see com.kroms.fribola.vo.ValueObject
     */
    public class EditionForm extends Form
    {
        public function EditionForm ()
        {
            super ();
        }

        /**
         * @inheritDoc
         */
        override protected function updateCurrent () : void
        {
            refresh();
        }

        /**
         * @inheritDoc
         */
        override protected function cancel () : void
        {
            revert();
            super.cancel(); // last as it triggers some event
        }

        /**
         * revert data changes back to initial values
         */
        protected final function revert () : void
        {
            current.revert();
            refresh();
        }

        /**
         * @inheritDoc
         */
        override protected function reset () : void
        {
            revert();
            validate();
            dispatchEvent( new FormEvent( FormEvent.RESET ) );
        }

        /**
         * refresh form fields from data
         */
        protected function refresh () : void
        {}

        /**
         * if data has been modified
         */
        public function get edited () : Boolean
        {
            return current && current.modified;
        }

        /**
         * retrieve initial data values
         */
        public function get source () : ValueObject { return current ? current.source : null; }

        /**
         * inheritDoc
         */
        override protected function get submitAllowed () : Boolean
        {
            var valid : Boolean = super.submitAllowed;
            return valid && edited;
        }

        /**
         * @inheritDoc
         */
        override protected function get resetAllowed () : Boolean
        {
            var filled : Boolean = super.resetAllowed;
            return filled && edited;
        }
    }
}
