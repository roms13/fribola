/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 21/02/2015
 * Time: 22:12
 */
package com.kroms.fribola.vo
{
    /*
     * provides a means to keep track of data modification time
     */
    public interface ITimestamp
    {
        function get created_at () : Date;
        function set created_at ( value : Date ) : void;
        function get last_updated_at () : Date;
        function set last_updated_at ( value : Date ) : void;
        function get last_sync_at () : Date;
        function set last_sync_at ( value : Date ) : void;
    }
}
