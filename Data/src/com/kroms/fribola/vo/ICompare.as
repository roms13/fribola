/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 21/02/2015
 * Time: 22:38
 */
package com.kroms.fribola.vo
{
    /*
     * provides a means to compare data
     */
    public interface ICompare
    {
        function equals ( data : ICompare ) : Boolean;
        function differs ( data : ICompare ) : Boolean;
    }
}
