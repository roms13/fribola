/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 21/02/2015
 * Time: 22:19
 */
package com.kroms.fribola.vo
{
    import mx.utils.ObjectUtil;

    /*
     * base class for value objects
     * all data related classes should extends this class
     */
    [RemoteClass(alias="com.kroms.fribola.vo.ValueObject")]
    public class ValueObject implements IVersion
    {
        private var _source : IVersion;

        /*
         * if data is not equals to another data
         * @parameter data : the data to compare with this instance
         */
        public final function differs ( data : ICompare ) : Boolean
        {
            return !equals(data);
        }

        /*
         * if data is equals to another data
         * @parameter data : the data to compare with this instance
         */
        public function equals ( data : ICompare ) : Boolean
        {
            return ( this === data );
        }

        /*
         * set this instance source data : allows for comparison with current data to see if the source has been modified in the meanwhile
         */
        [Transient]
        public function getSource () : IVersion { return _source; }

        [Transient]
        public function setSource ( value : IVersion, commit : Boolean = true ) : void
        {
            if ( _source == value )
                return;

            _source = value;

            // if you commit, it reverts to source (you've just set)
            // but it allows you also to modify source without erasing current user input(s)
            if ( commit )
            {
                revert();
            }
        }

        /*
         * instance source data : allows to define data at some point to compare later with
         */
        [Transient]
        public function get source () : ValueObject { return _source as ValueObject; }
        public function set source( value : ValueObject ) : void
        {
            if ( _source == value )
                return;

            setSource( value, true );
        }

        /*
         * if this instance has been modified (for example, user edited)
         */
        public function get modified () : Boolean
        {
            var noSource : Boolean = ( _source == null );
            var noCurrent : Boolean = ( this == null );

            return ( noSource && this ) || ( _source && noCurrent ) || ( this.differs( _source ) );
        }

        /*
         * makes a clean copy of this instance (with a new instance)
         */
        public function copy () : ValueObject
        {
            return ObjectUtil.copy( this ) as ValueObject;
        }

        /*
         * if all the value of this instance are empty
         */
        public function get empty () : Boolean
        {
            if ( !source )
                return true;

            return emptyData;
        }

        /*
         * TO BE OVERRIDEN : define if this instance can be considered as "empty" depending on its data on each inheritor (allows for fine tuning)
         */
        protected function get emptyData () : Boolean
        {
            return false;
        }

        /*
         * TO BE OVERRIDEN : if this instance pass form validation
         */
        public function get valid () : Boolean
        {
            return true;
        }

        /*
         * TO BE OVERRIDEN : used to hydrate instance with values from source
         */
        public function revert () : void
        {}

        public function commit () : void
        {
            setSource( this, true );
        }
    }
}
