/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 21/02/2015
 * Time: 22:38
 */
package com.kroms.fribola.vo
{
    /*
     * provides a means to compare, validate, commit, revert, reset data
     * useful for data manipulation and forms
     */
    public interface IVersion extends IValid, ICompare
    {
        function commit () : void;
        function revert () : void;
        function get modified () : Boolean;
        function getSource () : IVersion;
        function setSource ( data : IVersion, commit : Boolean = true ) : void;
    }
}
