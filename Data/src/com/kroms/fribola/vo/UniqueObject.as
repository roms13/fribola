/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 21/02/2015
 * Time: 22:22
 */
package com.kroms.fribola.vo
{
    import mx.utils.UIDUtil;

    public class UniqueObject extends ValueObject
    {
        private var _uid : String;

        override public function revert () : void
        {
            super.revert ();
            uid = sourceAsUniqueObject ? sourceAsUniqueObject.uid : null;
        }

        private function get sourceAsUniqueObject () : UniqueObject
        {
            return source as UniqueObject;
        }

        /*
         * uid auto-generated with UIDUtil
         */
        public function get uid():String { return _uid; }
        public function set uid(value:String):void
        {
            if ( _uid == value )
                return;

            _uid = value;
        }

        override protected function get emptyData () : Boolean
        {
            var emptyUID : Boolean = UIDUtil.isUID( _uid );
            return emptyUID;
        }
    }
}
