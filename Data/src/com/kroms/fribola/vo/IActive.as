/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 21/02/2015
 * Time: 22:03
 */
package com.kroms.fribola.vo
{
    /*
     * any data meant to be deactivable (instead of, for example, being merely deleted)
     * for filing or database row recycling
     */
    public interface IActive
    {
        function get active () : Boolean;
    }
}
