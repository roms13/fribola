/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 21/02/2015
 * Time: 22:33
 */
package com.kroms.fribola.vo
{
    /*
     * any timestampable and versionable data is considered as syncable
     */
    public interface ISync extends IVersion, ITimestamp
    {}
}
