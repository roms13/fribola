/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 21/02/2015
 * Time: 22:15
 */
package com.kroms.fribola.vo
{
    /*
     * any data meant to be translatable into multiple langages
     */
    public interface ITranslation
    {
        function get uid () : String;
        function get code () : String;
        function get denomination () : String;
    }
}
