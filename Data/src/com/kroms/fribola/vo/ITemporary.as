/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 21/02/2015
 * Time: 22:14
 */
package com.kroms.fribola.vo
{
    /*
     * any data meant to last for a given time
     */
    public interface ITemporary
    {
        function get duration () : int;
        function get ends_at () : Date;
    }
}
