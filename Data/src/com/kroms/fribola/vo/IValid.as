/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 21/02/2015
 * Time: 22:09
 */
package com.kroms.fribola.vo
{
    /*
     * provides a means to validate data
     * useful for data manipulation and forms
     */
    public interface IValid
    {
        function get valid () : Boolean;
    }
}
