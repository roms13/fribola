/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 21/02/2015
 * Time: 22:16
 */
package com.kroms.fribola.vo
{
    /*
     * any data that must be unique
     */
    public interface IUnique
    {
        function get uid () : String;
        function set uid ( value : String ) : void;
    }
}
