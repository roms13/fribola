/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 21/02/2015
 * Time: 22:12
 */
package com.kroms.fribola.vo
{
    public interface ISetting extends IActive
    {
        function set active ( value : Boolean ) : void;
    }
}
