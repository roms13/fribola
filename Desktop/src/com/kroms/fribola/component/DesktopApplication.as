/**
 * AS3 sourcecode development : Romain KELIFA
 * Date: 13/12/2015
 * Time: 16:26
 */
package com.kroms.fribola.component
{
    import com.kroms.fribola.impl.DefaultApplicationImplementation;
    import com.kroms.fribola.impl.DefaultJPEGEncoderImplementation;
    import com.kroms.fribola.impl.DefaultNetworkInfoImplementable;

    public class DesktopApplication extends CrossPlatformApplication
    {
        public function DesktopApplication ()
        {
            super ();
        }

        override protected function initializeLibraries():void
        {
            super.initializeLibraries();

            Application = DefaultApplicationImplementation.getInstance();
            JPEGEncoder = DefaultJPEGEncoderImplementation.getInstance();
            NetworkInfo = DefaultNetworkInfoImplementable.getInstance();

            Application.initialize();
            JPEGEncoder.initialize();
            NetworkInfo.initialize();
        }
    }
}
